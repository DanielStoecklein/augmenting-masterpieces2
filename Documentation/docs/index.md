 Augmenting Masterpieces

**Team:** <a href="https://jira.student.fiw.fhws.de:8443/secure/ViewProfile.jspa?name=k41016">Simon Weickert</a>, <a href="https://jira.student.fiw.fhws.de:8443/secure/ViewProfile.jspa?name=k42282">Haris Hodzic</a>, <a href="https://jira.student.fiw.fhws.de:8443/secure/ViewProfile.jspa?name=k45449">Daniel Stöcklein</a>, <a href="https://jira.student.fiw.fhws.de:8443/secure/ViewProfile.jspa?name=k46481">Thomas Hadgu</a>  

- [Motivation und Zielsetzung](#motivation-und-zielsetzung)
- [Analyse](#analyse)
    - [Was ist Augmented Reality?](#was-ist-augmented-reality)
    - [AR-Devices](#ar-devices)
    - [AR-Frameworks (markerless)](#ar-frameworks-markerless)
- [App-Overview](#app-overview)
    - [Verwendete Devices, IDEs, Frameworks, Assets](#verwendete-devices-ides-frameworks-assets)
    - [Architektur und Komponenten](#architektur-und-komponenten)
- [Installation](#installation)
- [Implementierung](#implementierung)
    - [Szenenwechsel](#szenenwechsel)
    - [Hauptmenü](#hauptmenü)
    - [Museum](#museum)
    - [Suchmen&#252;](#suchmenü)
    - [Rahmenauswahl](#rahmenauswahl)
    - [Flächenerkennung](#flächenerkennung)
    - [Bild Platzierung](#bild-platzierung)
    - [Bild Manipulation](#bild-manipulation)
    - [Skulptur Platzierung](#skulptur-platzierung)
    - [Skulptur Manipulation](#skulptur-manipulation)
    - [EventSystem](#eventsystem)
    - [Side-Menu](#side-menu)
- [Testing](#testing)
- [Bekannte Bugs](#bekannte-bugs)
- [Fazit](#fazit)
- [Ausblick](#ausblick)

---

# Motivation und Zielsetzung

Von: <a href="https://jira.student.fiw.fhws.de:8443/secure/ViewProfile.jspa?name=k45449">Daniel Stöcklein</a> 

Die Motivation des Projekts war es, den aktuellen Status des mobilen AR zu bewerten und herauszufinden, was möglich ist, welche technischen Voraussetzungen es gibt und welche Entwicklungsumgebungen/Frameworks zur Verfügung stehen.

Wir haben eine Proof-of-Concept-Anwendung für Android-Mobilgeräte entwickelt, mit der ein Benutzer ein Bild aus der Smartphone-Galerie oder aus dem Web auswählen und an einer Wand platzieren kann. Daraus ergaben sich folgende Zielsetzungen:

* Die App soll die reale Umgebungsstruktur erkennen können.
* Bilder sollen an einer virtuellen Wand augmentiert werden können.
* Es sollen mehrere vordefinierte Bilderrahmen auswählbar sein.
* Statuen sollen im Raum augmentiert werden können.
* Die augmentierten Objekte sollen interaktiv sein (Bewegung, Skalierung).
* Es sollen Screenshots der augmentierten Realität erstellt werden können.
* Bilder und Statuen sollen aus der Smartphone-Galerie oder aus dem Web ausgewählt werden können.
* Der User soll in einem virtuellen Museum die Bilder betrachten und herumlaufen können.

# Analyse

Von: <a href="https://jira.student.fiw.fhws.de:8443/secure/ViewProfile.jspa?name=k45449">Daniel Stöcklein</a> 

Nachfolgend sollen folgende Fragen beantwortet werden, um einen Überblick über die aktuellen Augmented-Reality-Technologien und den daraus resultierenden Herausforderungen für Hard- und Software zu erhalten.

* Was genau ist Augmented Reality?
* Wie funktionieren aktuelle Augmented Reality-Technologien (markerbased vs. markerless)?
* Welche Android-Mobilgeräte unterstützen Augmented Reality?
* Welche Augmented Reality-Frameworks sind verfügbar?

### Was ist Augmented Reality?

Von: <a href="https://jira.student.fiw.fhws.de:8443/secure/ViewProfile.jspa?name=k45449">Daniel Stöcklein</a> 

Augmented Reality (AR) ist die Erweiterung der realen Welt um virtuelle Objekte und Informationen. Virtuelle Objekte können in Echtzeit als 3D-Modelle direkt auf physische Objekte projiziert werden.

AR wird im Sichtfeld des Benutzers angezeigt. Um ein möglichst realistisches Erlebnis zu bieten, muss die Projektion neu berechnet und angezeigt werden, wenn sich das Sichtfeld ändert, z. B. eine Anpassung des Betrachtungswinkels oder eine Annäherung an ein Objekt.

**Abgrenzung zur Virtual Reality**

Im Gegensatz zur AR wird Virtual Reality (VR) eine vollständige virtuelle Umgebung erstellt. Dies führt zu einem signifikanten Unterschied zwischen AR und VR. In VR wird die Umgebung im Voraus modelliert, daher ist die vollständige Umgebung bereits zum Zeitpunkt der Entwicklung der Anwendung bekannt. In AR ist die Umgebung zum Zeitpunkt der Entwicklung nicht bekannt. Dies führt dazu, dass die Umgebung zur Laufzeit der Anwendung analysiert werden muss, um überhaupt eine Interaktion zu ermöglichen. Die Analyse der Umgebung kann auf viele verschiedene Arten gelöst werden.

**Markerbased-AR**

<img src="..\docs\Images\marker_based.png" alt="Markerbased AR" title="Markerbased AR"/>
<a href="https://thinkmobiles.com/blog/what-is-augmented-reality/" style="font-size:10px">img source</a>

Marker-basiertes AR verwendet ein bestimmtes visuelles Objekt – einen "Marker", das in der Benutzerumgebung platziert wird. Die Abmessungen und der Inhalt des Markers sind bereits während der Entwicklung der Anwendung bekannt. Wenn der Marker in der Umgebung erkannt wird, kann er als Referenzpunkt für die Platzierung weiterer virtueller Objekte im Sichtfeld des Benutzers dienen. Ein Marker kann eine beliebige Form haben, z. B. einen gedruckten QR-Code oder eine 1-Dollar-Note.

**Markerless-AR**

<img src="..\docs\Images\marker_less.png" alt="markerless AR" title="markerless AR"/>
<a href="https://www.analyticsinsight.net/future-ar-slam-technology-slam/" style="font-size:10px">img source</a>

Wie der Name schon sagt, benötigt markerless-AR keine Marker. Um dies zu ermöglichen, muss die Umgebung mit Hilfe der Kamera und anderer Sensoren so genau wie möglich analysiert werden. Bestimmte Feature-Punkte in der Umgebung werden erkannt und dienen als Referenzpunkte für virtuelle Objekte. Markerless-AR wird durch die kombinierte Verwendung von Kamera und Sensoren wie Kompass, Gyroskop und Beschleunigungsmesser ermöglicht. Tiefensensoren ermöglichen eine noch effizientere Erfassung der Umgebung.

<div style="page-break-after: always; "></div>

### AR-Devices

Von: <a href="https://jira.student.fiw.fhws.de:8443/secure/ViewProfile.jspa?name=k45449">Daniel Stöcklein</a> 

AR-Mobilgeräte können in verschiedene Kategorien unterteilt werden. Die beiden Hauptkategorien sind Head-Mounted- (z.B. Datenbrille) und Handheld-Geräte (Smartphones). Der Schwerpunkt dieses Projekts lag auf mobilen Handheld-Geräten. Nachfolgend ein kurzer Überblick über die technischen Voraussetzungen von Android-Mobilgeräten für die Verwendung von Markerless-AR.

**Android**

| ARCore                               |                                                                                                                  |
| ------------------------------------ | ---------------------------------------------------------------------------------------------------------------- |
| Plattform                            | Android 7.0 oder neuer                                                                                           |
| Herstellermodelle                    | <a href="https://developers.google.com/ar/discover/supported-devices">Komplette Liste<a>                         |
| Anzahl der Geräte mit ARCore Support | > 400 Millionen <a href="https://arinsider.co/2019/05/13/arcore-reaches-400-million-devices/"> arinside.co, 2019 |

### AR-Frameworks (markerless)

Von: <a href="https://jira.student.fiw.fhws.de:8443/secure/ViewProfile.jspa?name=k45449">Daniel Stöcklein</a> 

**Android**-Mobilgeräte ermöglichen markerless-AR mithilfe des von Google entwickelten **ARCore**-Frameworks.

| ARCore             |                                                                                                                                                                                                                                                                                    |
| ------------------ | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Features           | <ul><li>Motion/Orientation Tracking</li><li>Environmental Understanding</li><li>Light Estimation </li><li>User interaction</li><li>Horizontal Plane Detection</li><li>Vertical Plane Detection</li><li>Augmented Images</li><li>Cloud Anchors</li><li>...und vieles mehr</li></ul> |
| Veröffentlicht     | August 2017                                                                                                                                                                                                                                                                        |
| Aktuelle Version   | 1.17.0                                                                                                                                                                                                                                                                             |
| Programmiersprache | C, C++, Java, Kotlin                                                                                                                                                                                                                                                               |

**Unity** stellt ein AR-Framework zur Verfügung, welches ARCore und ARKit zusammenführt und somit eine Entwicklung für beide Betriebssysteme ermöglicht.

| AR Foundation      |                                                                                                                                                                                                                                     |
| ------------------ | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Features           | <ul><li>Motion/Orientation Tracking</li><li>Environmental Understanding</li><li>Light Estimation </li><li>User interaction</li><li>Horizontal Plane Detection</li><li>Vertical Plane Detection</li><li>...und vieles mehr</li></ul> |
| Veröffentlicht     | May 2017                                                                                                                                                                                                                            |
| Aktuelle Version   | 3.1.3                                                                                                                                                                                                                               |
| Programmiersprache | C#                                                                                                                                                                                                                                  |

---

# App-Overview

Von: <a href="https://jira.student.fiw.fhws.de:8443/secure/ViewProfile.jspa?name=k45449">Daniel Stöcklein</a> 

In diesem Abschnitt möchten wir einen Überblick geben, welche Technologien für die Entwicklung der App verwendet wurden und warum. Die App verwendet markerless-AR, da die Umgebung unbekannt und somit kein Referenzpunkt für die Platzierung der Bilder vorhanden ist. Wie bereits erwähnt soll die App die Umweltumgebung selbständig analysieren können, somit kann auf einen vordefinierten Marker verzichtet werden.

### Verwendete Devices, IDEs, Frameworks, Assets

Von: <a href="https://jira.student.fiw.fhws.de:8443/secure/ViewProfile.jspa?name=k45449">Daniel Stöcklein</a>

**Devices**

Zunächst ein kurzer Überblick über die bei der Entwicklung und beim Testen verwendeten Mobilen Geräte:


| Model           | Betriebssystem |
| --------------- | -------------- |
| Pixel 2XL       | Android 10     |
| Samsung S8 Edge | Android 10     |
| Huawei P30 Lite | Android 10     |
| Samsung S8      | Android 9      |
| Samsung S10     | Android 10     |

**IDE**

Unity wird bereits häufig für Spiel- und 3D-Anwendungen verwendet, was es zu einem guten Ausgangspunkt für die Entwicklung von AR-Anwendungen macht. Wir haben uns bewusst für die Unity Version 2019.3.7f1 entschieden, welche Plattformübergreifend und vor allem in Verbindung mit AR Foundation am stabilsten läuft.

| IDEs                                                               | Version    |
| ------------------------------------------------------------------ | ---------- |
| <a href="https://unity.com/products/core-platform">Unity Editor<a> | 2019.3.7f1 |
| <a href="https://developer.android.com/studio"> Android Studio<a>  | 3.6.3      |

**Framework**

Wir haben beschlossen, die App mit dem **AR Foundation** Unity-Framework zu erstellen. Dies ermöglicht uns eine plattformübergreifende Unterstützung für Android und (zukünftig) iOS Geräte. Das Framework umfasst Funktionalitäten von ARKit und ARCore. 

| Frameworks                                                                                                                                                                                                                   | Version |
| ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------- |
| <a href="https://unity.com/de/unity/features/arfoundation">AR Foundation<a> <ul><li><a href="https://developer.apple.com/augmented-reality/">ARKit<a></li><li><a href="https://developers.google.com/ar">ARCore<a></li></ul> | 3.1.0   |

**Assets**

Der Unity Asset Store bietet eine große Bandbreite an Erweiterungen für die mobile App-Entwicklung. Die App verwendet folgende Assets:

| Assets                                                                                                                                      | Version |
| ------------------------------------------------------------------------------------------------------------------------------------------- | ------- |
| <a href="https://assetstore.unity.com/packages/templates/systems/poly-toolkit-104464">Poly Toolkit<a>                                       | 1.1.2   |
| <a href="https://assetstore.unity.com/packages/tools/gui/swipe-menu-45977">Swipe Menu<a>                                                    | 1.2     |
| <a href="https://assetstore.unity.com/packages/tools/gui/simple-side-menu-143623">Simple Side Menu<a>                                       | 1.0.3   |
| <a href="https://assetstore.unity.com/packages/tools/input-management/lean-touch-30111">Lean Touch<a>                                       | 2.1.2   |
| <a href="https://assetstore.unity.com/packages/tools/integration/native-gallery-for-android-ios-112630">Native Gallery for Android & iOS<a> | 1.4.0   |
| <a href="https://assetstore.unity.com/packages/3d/environments/showroom-environment-73740"> Showroom Environment<a>                         | 1.1     |
| <a href="https://assetstore.unity.com/packages/tools/input-management/ultimate-joystick-27695"> Ultimate Joystick<a>                        | 2.6.1   |
| <a href="https://assetstore.unity.com/packages/essentials/asset-packs/standard-assets-for-unity-2017-3-32351"> Standard Assets<a>           | 1.1.5   |

**Packages**

| Packages                                                                   | Version |
| -------------------------------------------------------------------------- | ------- |
| [Json.Net for Unity](https://github.com/jilleJr/Newtonsoft.Json-for-Unity) | 12.0.3  |

### Architektur und Komponenten

Von: <a href="https://jira.student.fiw.fhws.de:8443/secure/ViewProfile.jspa?name=k45449">Daniel Stöcklein</a>

Die folgende Grafik beschreibt die Architektur der App sowie die dazugehörigen Komponenten. Die Komponenten sind hierarchisch vom App Code bis zur ausführenden Plattform aufgelistet. Die Farbe blau beschreibt C#-Code. Die App ist komplett in C# geschrieben, um Plattformunabhängig ausführbar zu sein. Das Modul **Augmented Reality & 3D UI** beinhaltet Code, welcher u.a. für das Museum, die Objekt-Manipulation und Objekt-Instanziierung zuständig ist. Das Modul **2D UI** beschreibt die Menüführung, die Bildersuche sowie die Bilderrahmenauswahl. **ARFoundation** umfasst sämtlichen Code zur Umgebungserkennung. Das Modul **Assets** beschreibt ergänzenden Code, welcher der App aus dem Unity-Store hinzugefügt wurde.

<img src="..\docs\Daniel\architecture.png" title="Einstellungen Android" width="45%"/>

Grafik: <a href="https://jira.student.fiw.fhws.de:8443/secure/ViewProfile.jspa?name=k45449">Daniel Stöcklein</a> 

Die Komponente **Runtime** beschreibt die UnityEngine, welche für die Ausführung der App zuständig ist. *Mono* ist eine .NET Entwicklungsplattform inkl. Compiler, mit der Entwickler u.a. Anwendungen für Android Geräte in C# erstellen können. 

**Platform** umfasst Endgeräte bzw. Betriebssysteme auf denen die App lauffähig ist.

---

# Installation
Von: <a href="https://jira.student.fiw.fhws.de:8443/secure/ViewProfile.jspa?name=k45449">Daniel Stöcklein</a>

Wie bereits im Abschnitt [AR-Devices](#ar-devices) erwähnt, ist für die Nutzung der App ein AR kompatibles Android-Mobilgerät in der Version 7.0 oder neuer erforderlich. Die Installationsdatei (.apk) kann **hier** runterladen werden.

Zunächst müssen auf dem Smartphone geringfügige Vorbereitungen vorgenommen werden. Diese betreffen die Sicherheitseinstellungen und erlauben es auf dem Smartphone Apps unbekannter Herkunft zu installieren.

* Smartphone-Einstellungen öffnen.
* Menüpunkt "Sicherheit" -> "Geräteverwaltung" auswählen und die Einstellung "Unbekannte Herkunft" aktivieren.

<img src="..\docs\Images\apk_einstellungen_android.jpg" title="Einstellungen Android" width="35%"/>

Die heruntergeladene .apk-Datei kann nun geöffnet und installiert werden.

---

# Implementierung

### Szenenwechsel

Von: [Thomas Hadgu](https://jira.student.fiw.fhws.de:8443/secure/ViewProfile.jspa?name=k46481)

<big><big>Szenen</big></big>

In der App gibt es  6 verschiedene Szenen:

| Szene                       | Beschreibung                                                                             |
| --------------------------- | ---------------------------------------------------------------------------------------- |
| **MainMenu**                | Das Hauptmenü wird nach dem Start der App als erstes angezeigt.                          |
| **Museum**                  | Erlaubt das Betrachten besonders geschätzter Bilder in einer Museumsumgebung.            |
| **SearchMenu**              | In dieser Szene kann ein Bild oder ein 3d-Objekt führ die AR-Umgebung ausgewählt werden. |
| **FrameSelection**          | Hier kann ein Rahmen für die anzuzeigenden Bilder bestimmt werden.                       |
| **ARSculpture**             | In dieser Szene können 3d-Skulpturen in den Raum projiziert werden.                      |
| **Augmenting Masterpieces** | Hier werden die ausgewählten Bilder mit Rahmen im Raum abgebildet.                       |


**Übersicht der Szenenwechsel**

<img src="..\docs\Thomas\Szenendiagramm.jpg" alt="Szenendiagramm" title="Szenendiagramm" width=80%/>

Grafik: <a href="https://jira.student.fiw.fhws.de:8443/secure/ViewProfile.jspa?name=k46481">Thomas Hadgu</a> 
  
<big><big>Unity-Methoden</big></big>

Wird mit der Methode
[`SceneManager.LoadScene`](https://docs.unity3d.com/ScriptReference/SceneManagement.SceneManager.LoadScene.html)
eine Szene geladen, dann wir die Szene in genau den Zustand gesetzt der beim Builden der Applikation für diese definiert wurde. Wurde die Szene im Lauf einer Session schon einmal geladen, aber dann wieder verlassen, wird nicht der zuvor erreichte Zustand der Szene wiederhergestellt, sondern der Ursprungszustand wird wieder angenommen. Da `SceneManager.LoadScene` beim Laden einer neuen Szene die alte Szene mit allen Game Objects zerstört, kann so kein Szenenwechsel stattfinden ohne den Zustand der Szene zu verlieren.

```csharp
// Szene mit Name "B" wird einfach geladen 
 SceneManager.LoadScene("B");
// -> Ausschließlich  Szene B wird angezeigt
```


Das *additive* Laden einer Szene ermöglicht die Methode `SceneManager.LoadScene` durch die Übergabe des Parameters `LoadSceneMode.Additive`. Dabei wird die neue Szene in die bestehende Szenenumgebung geladen und wird inaktiv gesetzt. Dabei gilt zu beachten, dass die Game Objects der inaktiven Szenen weiterhin ansprechbar und in der Applikation sichtbar sind, solange sie selbst nicht deaktiviert werden.

```csharp
// Szene mit Name "B" wird additiv geladen 
SceneManager.LoadScene("B", LoadSceneMode.Additive);
// -> Szene "B" ist nun geladen aber inaktiv
```


Will man einzelne Game Objects von einer Szene in eine andere übertragen, ermöglicht die Methode 
[`Object.DontDestroyOnLoad`](https://docs.unity3d.com/ScriptReference/Object.DontDestroyOnLoad.html) 
das Zerstören dieses Game Objects beim Entladen einer Szene zu verhindern. Das Objekt ist dabei auch in den nächsten Szenen persistent und bleibt so lange bestehen, bis die App neugestartet oder das Objekt mittels Script zerstört wird.

```csharp
// Das Game Object "gameObject bleibt beim nächsten Szenen Wechsel erhalten"
DontDestroyOnLoad(gameObject);
```

<big><big>Vorgehensweise</big></big>

In **Augmenting Masterpieces** wird der Szenenwechsel mit verschiedenen Komponenten bewerkstelligt.

**Holder Objekt**

Das Holder Objekt ist ein Game Object, das in der Hierarchie einer Szene dieser direkt zugeordnet ist. Alle Game Objects, die nach einem Szenenwechsel nicht mehr angezeigt werden sollen, sind dem Holder Objekt* als *children* untergeordnet. Wird die Szene gewechselt, so wird das Holder Objekt auf inaktiv gesetzt und alle Nachfahren erben diesen Zustand, wodurch sie nicht mehr sichtbar sind.


**Manager Objekt**

Das Manager Objekt ist dafür zuständig, den Szenenwechsel über die aktive Zeit einer Szene hinaus weiter zu führen.
Der Ablauf stellt sich wie folgt dar:

<img src="..\docs\Thomas\SzenenWechsel.jpg" alt="Szenenwechsel" title="Szenenwechsel" width=80%/>

Das Manager Objekt wird in der Hauptmenüszene einmalig geladen, da dies die erste Szene ist, welche der Nutzer besucht. 

Dem Manager Objekt ist einzig das Skript `LoadScene` als Komponente zugewiesen. Da das Manager Objekt nicht deaktiviert werden darf, wenn eine Szene zerstört wird, wird direkt bei seiner Erstellung `Object.DontDestroyOnLoad` ausgeführt. Zeitgleich wird dabei dem Event `SceneManager.sceneLoaded` eine Action hinzugefügt die bei jedem Laden einer Szene die Methode `SceneLoader.ActivateYourScene` ausführt.

```csharp 
public class SceneLoader : MonoBehaviour
{
    private void Start()
    {
        SceneManager.sceneLoaded += Action_SceneLoaded;
        DontDestroyOnLoad(this);
    }
    ...
    public void Action_SceneLoaded(Scene LoadedScene, LoadSceneMode mode)
    {
        ActivateYourScene(LoadedScene.name);
    }
    ...
}
```

Wird über das Skript `SceneInit` die Methode `SceneLoader.LoadScene` ausgeführt, dann wird überprüft, ob eine Szene mit dem übergebenen Namen besteht. Ist dies nicht der Fall so wird die angegebene Szene *additiv* geladen und das zuvor bearbeitete Event `SceneManager.sceneLoaded` wird ausgelöst. Dadurch wird in jedem Fall die Methode `SceneLoader.ActivateYourScene` aufgerufen.


Wird die Methode `SceneLoader.ActivateYourScene` aufgerufen, dann werden zuerst in der aktuellen Szene diejenigen Objekte, die nicht persistent aktiv bleiben sollen in der Methode `SceneLoader.SetRootObject` deaktiviert. Danach wird die übergebene Szene aktiviert und folglich werden die deaktivierten Objekte der neuen Szene wieder aktiv gesetzt.
Um nicht-persistente Objekte von persistenten zu unterscheiden, wurden diese in ein Holder Objekt untergeordnet. Dies trifft aber nicht für Objekte, die erst zur Laufzeit entstehen, zu. Daher werden sie auch in der `SceneLoader.SetRootObject` behandelt.

```csharp
public void ActivateYourScene(string sceneName)
{
    string lastScene = SceneManager.GetActiveScene().name;
    SetRootObjects(false, sceneName, lastScene);
    SceneManager.SetActiveScene(SceneManager.GetSceneByName(sceneName));
    activeScene = SceneManager.GetActiveScene().name; 
    SetRootObjects(true, sceneName, lastScene);

    //Daniel Stöcklein
    if (activeScene == "Augmenting Masterpieces") SetARState("pic");
    if (activeScene == "ARSculpture") SetARState("poly");
}
```

Sollte die neue Szene eine der AR-Szenen sein dann wird im Anschluss die Methode `SetARState` aufgerufen die das Sidemenu schließt.

**SceneInit Skript**

Das Skript `SceneInit` verbindet das Manager Objekt mit dem Szenenelement, das den Szenenwechsel auslöst. Meist ist es dazu einem Button-Object als Komponente zugewiesen und die Methode `SceneInit.loadSceneLoader` wird bei Auslösung ausgeführt. `SceneInit` wird benötigt, da das Manager Objekt erst während der Laufzeit der Applikation in einer Szene ansprechbar ist. Somit ist im Editor kein direkter Verweis auf das Manager Objekt möglich. 

```csharp 
public class SceneInit : MonoBehaviour
{
    public void loadSceneLoader(string sceneName)
    {
        GameObject sceneManager = GameObject.FindGameObjectWithTag("Manager");
        SceneLoader sceneLoader = sceneManager.GetComponent<SceneLoader>();
        sceneLoader.LoadScene(sceneName);
    }
}
```
Für die Rahmenauswahl wird `SceneInit` direkt im Skript `TouchHandler` des Assets `SwipeMenu` aufgerufen, damit es beim Antippen eines Rahmens richtig ausgelöst wird. `SceneInit` ist dem Game Object Menu in der Rahmenauswahlszene zugeordnet.



### Hauptmenü 

Von: <a href="https://jira.student.fiw.fhws.de:8443/secure/ViewProfile.jspa?name=k45449">Daniel Stöcklein</a>

Nach Start der App gelangt der Nutzer direkt in das Hauptmenü. 
**Hinweis:** Die App wechselt automatisch in den Landscape-Mode.

<img src="..\docs\Daniel\MainMenu.png" title="Hauptmenü" width="85%"/>

Im Hauptmenü stehen zwei Menüs zur auswählen, welche durch einen Finger-Tap aufrufbar sind. Der Nutzer kann jederzeit durch Touch auf den Zurück-Button zum vorherigen Menü zurückkehren. Wichtig ist hierbei der Abschnitt [Szenenwechsel](#szenenwechsel) zu beachten.

* Mein Museum 
  * Der Nutzer kann in einem virtuellen Museum eigene Bilder platzieren und herumlaufen.
* Augmented Reality starten
  * Die App führt den Nutzer automatisch durch die jeweiligen Untermenüs, um Bilder oder Statuen zu augmentieren.
  
Der **Zurück-Button** links oben im Hauptmenü beendet die App.

### Museum 

Von: <a href="https://jira.student.fiw.fhws.de:8443/secure/ViewProfile.jspa?name=k42282">Haris Hodzic</a> 

<img src="..\docs\Haris\Museum.jpg" title="Hauptmenü" width="85%"/>

Das Museum ist ein 3D-Raum, in dem der Benutzer seine im AR-Modus gespeicherten Bilder an vordefinierten Bildflächen platzieren kann. Der Museumsraum verfügt dabei über 16 solcher Bildflächen. Der Nutzer bewegt sich mit einem First Person Character und zwei Joysticks durch den Raum und kann die Bildflächen über Raycasting auswählen. Durch den Touch auf einer dieser Bildflächen öffnet sich eine Vorschau, bei der die gespeicherten Bilder angezeigt. Wählt er diese wird das Bild auf der großen Bildfläche angezeigt.

<img src="..\docs\Haris\Aktivitaetsdiagramm.png" title="Museum" width="85%"/>

<img src="..\docs\Haris\Museum_Klassendiagramm.png" title="Museum" width="100%"/>

<big><big>Tutorial</big></big>

Das Tutorial ist eine schlichte Textanzeige, welche dem User die Funktionen im Museum erklärt. Dies wird über eine Koroutine realisiert. Eine Koroutine ist eine Verallgemeinerung einer Prozedur oder Funktion. Durch sie kann die Ausführung des Codes pausiert werden und in einem anderen Frame an derselben Stelle fortgesetzt werden. Damit wird realisiert, dass der Text alle 2 oder 3 Sekunden sich ändert und die Joysticks eingeblendet werden.

**Script: ShowWelcomeTxt.cs**
```csharp 
private IEnumerator showWelcomeMessage() 
{
    yield return new WaitForSeconds(2f);
    welcomeTxt.text = "Mit den Joysticks links und rechts können Sie sich im Museum bewegen";
    yield return new WaitForSeconds(2f);
    Joystickcanvas.SetActive(true);
    yield return new WaitForSeconds(2f);
    welcomeTxt.text = "Durch klicken auf eine Wand erscheint die Bilderauswahl";
    yield return new WaitForSeconds(3f);
    welcomeTxt.text = "Durch auswahl eines Bildes erscheint dieses auf Ihrer gewählten Wand";
    yield return new WaitForSeconds(3f);
    TutorialObj.SetActive(false);
    PlayerPrefs.SetString("Tutorial", "Finished");
}
```
Das Tutorial wird dabei nur einmal angezeigt. Dafür werden die Playerprefs genutzt. Damit können Werte dauerhaft gespeichert werden. Dies wird über Schlüssel und Schlüsselwerte realisiert. Hier wird der Schlüssel "Tutorial" und der Schlüsselwert "Finished" gespeichert.

In der Startmethode wird dann abgefragt ob der Schlüssel existiert. Tut er das nicht wird die Koroutine gestartet. Andernfalls werden die Joysticks eingeblendet.

```csharp 
public void Start()
{
    if(PlayerPrefs.HasKey("Tutorial") == false)
    {
        StartCoroutine(showWelcomeMessage());
        
    } else
    {
        TutorialObj.SetActive(false);
        Joystickcanvas.SetActive(true);
    }
}
```

<big><big>Bewegen im Museum</big></big>

<img src="..\docs\Haris\MovementMuseum.gif" title="Museum" width="85%"/>

Der First Person Controller wird über Joysticks bewegt, wobei der Linke für die Bewegung zuständig ist und der Rechte für die Kamera. Beim First Person Character handelt es sich um ein 3D-Objekt in Form einer Kapsel, dem ein Charakter Controller und ein Rigidbody angefügt ist. Ein Rigidbody ist die Hauptkomponente, die das physische Verhalten eines GameObject ermöglicht. 

Bezüglich der Empfindlichkeit der Kamera erfolgte eine Anpassung auf den Museumraum. Die Empfindlichkeit auf der X-Achse wurde erhöht, um zügige Kamerabewegungen zu ermöglichen, während die Empfindlichkeit auf der Y-Achse reduziert wurde, da ein zügiges Auf- und Abschauen nicht nötig ist.

<big><big>PreviewLoader</big></big>

Eine Vorschau aller Verfügbaren Bilder wird angezeigt, sobald ein Raycasthit eine der Rahmenflächen trifft. Die Vorschau basiert auf zwei Panels (Parent und Child Panel) und dem Prefab small_Picture, welches als Childelement ein Button hat.
* Das Parent Panel hat als zusätzliche Komponente ein Scroll Rect.  
* Das Child Panel hat als zusätzliche Komponente einen Content Size Fitter und eine Horizontal Layout Group. 

Das Scroll Rect dient dazu horizontal durch die Bilder zu wischen. Mit dem Content Size Fitter wird weiterhin die Größe der Bilder kontrolliert und mit der Horizontal Layout Group wird dafür Sorge getragen, dass die Elemente nebeneinander angefügt werden.

Die Anzahl der Instanzen des Prefabs wird durch die Elementanzahl der Liste pictures festgelegt. Bei den Listelementen handelt es sich um Sprites (2D-Grafikobjekte). Diese werden dem Source Image des Childelements zugeordnet.

**Script: TurnTableScript.cs**
```csharp
public void Start()
    {
        List<Sprite> pictures = ProcessImages();

        for (int i = 0; i < pictures.Count; i++)
        {
            GameObject clone  = (GameObject)Instantiate(smallPicture);
            clone.GetComponentInChildren<Image>().sprite = pictures[i];
            clone.transform.SetParent(picturePanel);
            clone.name = "Clone" + i;
        }
    }
```

Um die Liste mit den Sprites zu erhalten werden alle Bilddateien zeilenweiße ausgelesen und in einer Liste überführt. Aus ihnen werden dann 2D-Texturen erstellt, mit dem wiederum die Sprites erstellt wird. Die Methode hat als Rückgabewert dann die vollständige Liste.
```csharp
public static List<Sprite> ProcessImages()
    {
        string txtPath = Application.persistentDataPath + "/" + "MaterialForMuseum.txt";
        List<string> picture_name_list = new List<string>();

        StreamReader sr = new StreamReader(txtPath);
        string text;
        
        while ((text = sr.ReadLine()) != null){

            picture_name_list.Add(text);
        } 

        List<Sprite> list = new List<Sprite>();
        foreach (string obj in picture_name_list)
        {
            byte[] bytes = File.ReadAllBytes(obj);
            Texture2D texture = new Texture2D(2, 2);
            texture.LoadImage(bytes);
            Sprite sprite = Sprite.Create(texture, new Rect(0.0f, 0.0f, texture.width, texture.height), new Vector2(0f, 0f), 100f);
            list.Add(sprite);
        }
        return list;
    }
```

<big><big>Auswahl und Platzierung der Bilder</big></big>

<img src="..\docs\Haris\placePicture.gif" title="Museum" width="85%"/>

Die Auswahl der Bildflächen wird über Raycasting realisiert, da die Wandflächen Gameobjects sind und keine UI-Elemente beinhalten können, wie z.B. eine Buttonkomponente. 
Ein Raycast ist im Wesentlichen ein Strahl, welcher von einem Ursprung ausgesendet und sich in eine bestimmte Richtung bewegt und bei einem Treffer reagiert. 
-> Wenn der Benutzer eine der Wandflächen berührt, öffnet sich die Bildervorschau. 
Dabei wurde die Distanz, wie weit der Strahl gehen kann, festgelegt. Damit wird verhindert, dass die Vorschau sich versehentlich öffnet und nur nahe Wandflächen ausgewählt werden können.

<img src="..\docs\Haris\Raycast.png" title="Museum" width="85%"/>

**Script: RaycastPicture.cs**
```charp
public void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray,out RaycastHit hit, 6))
            {
                if (hit.transform.name == "Paint") 
                { 
                    if (panel.gameObject.activeSelf == false)
                    {
                        panel.gameObject.SetActive(true); 
                    } else
                    {
                        panel.gameObject.SetActive(false);
                    }
                }
            }
        }
        staticRaycast.Update();
    }
```

StaticRaycast ist ähnlich zu RaycastPicture. Es handelt sich dabei um eine statische Klasse. Sie dient dazu, den Namen des Parent-Objekts global zur Verfügung zu haben. Dies ist dahingehend notwendig, um das ausgewählte Bild auf die Bildfläche zu übertragen.

**Script: staticRaycast.cs**
```csharp
public static string Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out RaycastHit hit))
            {
                if (hit.transform.name != "Paint")
                {
                  
                } else
                {
                    name = hit.transform.parent.parent.name;
                }
            }
        }
        return name;
    }
}
```

<big><big>Dauerhafte Speicherung der Bilder</big></big>

Die dauerhafte Speicherung der Bilder erfolgt über die Playerprefs. Das Script SavePicturePersistent ist dabei dem Prefab smallPicture angefügt. Das Source Image (Sprite) des instanziierten Prefabs wird dabei ins PNG-Format kodiert und in einem Byte-Array gespeichert. Dieses wird benötigt, um das Bild in einem String zu konvertieren, da über die Playerprefs nur int, floats und Strings gespeichert werden können.
Diesem String wird dann der Parent-Objektname als Schlüssel übergeben und als Schlüsselwert das Bild als String.

**Script: SavePicturePersistent.cs**
```csharp
public void SavePictureToPlayerPref()
    {
        string name = StaticRaycast.Update();
        paint = GameObject.Find(name).transform.Find("Holder").transform.Find("Paint").gameObject;
        
        Texture2D texture = btn.GetComponent<Image>().sprite.texture as Texture2D;
        byte[] texAsByte = texture.EncodeToPNG();
        string texAsString = Convert.ToBase64String(texAsByte);
        string myKey = name;
        PlayerPrefs.SetString(myKey, texAsString);
        PlayerPrefs.Save();

        paint.GetComponent<Renderer>().material.SetTexture("_MainTex", texture);
    }
```

Damit die platzierten Bilder auch nach einem Neustart der App angezeigt werden, muss der Vorgang in SavePicturePersistent rückgängig gemacht werden, da die Bilder nur als String gespeichert sind. Dabei wird geprüft ob der Schlüssel vorhanden ist.

**Script: PlayerPrefImageLoad.cs**
```csharp
void Start()
    {
        Texture2D texture = new Texture2D(1024, 768);
        if (PlayerPrefs.HasKey(paint.transform.parent.transform.parent.name))
            
        {
            string texAsString = PlayerPrefs.GetString(paint.transform.parent.transform.parent.name);
            byte[] texAsByte = Convert.FromBase64String(texAsString);
            texture.LoadImage(texAsByte);
        }
        paint.GetComponent<Renderer>().material.SetTexture("_MainTex", texture);
    }
```

<big><big>Museum zurücksetzen</big></big>

<img src="..\docs\Haris\resetMuseum.gif" title="Museum" width="85%"/>

Um alle Bildflächen zurückzusetzen, müssen alle gespeicherten Schlüssel gelöscht und dieser Stand dann gespeichert werden. Da dies das Tutorial beinhaltet, muss dieser wieder erneut gespeichert werden, damit dies nicht wieder durchlaufen werden muss. Damit der Reset auch für den Benutzer sichtbar ist, wird die Museumszene erneut geladen. Dadurch verschwinden alle Bilder auch von den Bildflächen.

**Script: ResetPlayerPrefKeys**
```csharp
  public void resetKeys()
    {
        PlayerPrefs.DeleteAll();
        PlayerPrefs.Save();
        PlayerPrefs.SetString("Tutorial", "Finished");
        SceneManager.LoadScene("Museum");
    }
```

### Suchmen&#252;
Von: [Thomas Hadgu](https://jira.student.fiw.fhws.de:8443/secure/ViewProfile.jspa?name=k46481)

Das ***Suchmenü*** ist in drei Tabs aufgeteilt, die sich mit ihren Aufgaben beschäftigen.

* Eigene Gallerie
* Unsplash Suche
* Poly Suche

Im Editor sind die Tabs als Game Objects organisiert, deren Nachfahren jeweils die GUI-Elemente der Tabs sind. Tab-Wechsel werden durch das Aktivieren und Deaktivieren der jeweiligen Objekte vollzogen, wenn auf einen der Tab-Buttons am unteren Rand der Benutzeroberfläche gedrückt wird.

Gelangt der Nutzer das erste Mal in das Suchmenü, befindet er sich in der Unsplash Suche.


<img src="..\docs\Daniel\SearchMenu.png" title="Hauptmenü" width="85%"/>

Von jedem der Tabs aus kommt man mit dem Knopf in der oberen linken Ecke zurück in das Hauptmenü.

<big><big>Auswahl eigener Bilder</big></big> 

Im Tab für die Auswahl eigener Bilder ist beim ersten Betreten nur ein Knopf mit der Aufschrift *Eigene Bilder* zu sehen. 

Wird dieser gedrückt wird die Methode `GalleryScript.PickImage` ausgeführt. Diese Ruft dann die Methode `NativeGallery.GetImageFromGallery` des Assets [**NativeGallery**](https://github.com/yasirkula/UnityNativeGallery) auf. Daraufhin wird ein weiteres Fenster aufgerufen und der Nutzer kann ein Bild aus seiner Galerie auswählen. Kehrt der Nutzer nach der Auswahl in die Applikation zurück, so wird im `GalleryScript.HandleMediaPickCallback` das Vorschaubild und der Knopf zur Rahmenauswahl aktiviert und die Textur des Vorschaubilds mit dem ausgewählten Bild befüllt.

```csharp
public void PickImage()
{
    NativeGallery.GetImageFromGallery(HandleMediaPickCallback, "Pick Image for the AR Frame");
}

private void HandleMediaPickCallback(string path)
{
    Texture2D image = NativeGallery.LoadImageAtPath(path);
    GoToFramesButton.SetActive(true);
    preview.SetActive(true);
    preview.GetComponent<RawImage>().texture = image;
    AdjustImageSize(image, preview.GetComponent<RawImage>());
}
```
   
<img src="..\docs\Thomas\ScreenshotGallerieEnde.jpg" title="Eigenes Bild wurde ausgewählt" width="80%"/>

<big><big>Internetsuche</big></big> 

In den Tabs **Poly Suche** und **Unsplash Suche** wird die Suche nach Werken Anderer, über die [Poly Api](https://developers.google.com/poly/develop/api) für 3D-Skulpturen bzw. die [Unsplash Api](https://unsplash.com/documentation) für Fotografien.

Die Interaktion mit der Poly API läuft hauptsächlich über das [Poly Toolkit](https://developers.google.com/poly/develop/toolkit-unity). Dieses enthält Klassen und Methoden, die es erlauben 3D-Objekte von Poly in die Applikation auch während der Laufzeit zu übertragen. Die Schnittstelle von Poly Toolkit zur Applikation `PolyToolkit.PolyApi` wird mit dem ersten Laden des `PolyRequestHandler` Skripts initialisiert. Ohne dies ist keine Interaktion mit dem Poly Toolkit möglich. Zur korrekten Authentifizierung wird ein *Acces Key* benötigt. Dieser wird in den Einstellungen zum Asset übergeben.

***Anmerkung*** *Poly Toolkit wird aktuell nicht mehr unterstützt. Während der Entwicklung waren die benutzten Methoden funktionsfähig und führten zu keinen größeren Problemen. Jedoch kann sich das in Zukunft jederzeit mit einer Veränderung der Schnittstellen von Poly oder Unity ändern.*

Für die Kommunikation mit der Unsplash API wird ebenfalls ein *Access Key* benötigt. Dieser wird in dem Skript `UnsplashRequestHandler` abgespeichert.

Im Allgemeinen wird bei der Suche nach dem folgenden Muster vorgegangen:

* Eingabe des Suchbegriffs in eine **Suchleiste**
* Übergabe des Suchbegriffs an einen **RequestHandler**
* Im **RequestHandler** wird der Suchbegriff an die jeweilige API übergeben
* Als Antwort erhält man eine Liste an Suchergebnissen die an einen **ImageLoader** übergeben werden
* Der **ImageLoader** geht durch die einzelnen Suchergebnisse in den Listen und lädt deren **Thumbnails** herunter und speichert sie mit den Daten zu dem Ergebnis als Resultat ab 
* Die **Thumbnails** werden dann in einer Scrollleiste in der Benutzeroberfläche angezeigt und fungieren als Buttons
* Durch das Herabscrollen der Liste können weitere Ergebnisse von der API angefragt werden
* Wird auf eines der **Thumbnails** geklickt, wird die **Preview** zu diesem Werk aufgerufen
* In der **Preview** wird das Werk in der Form heruntergeladen, in der es in den folgenden Szenen weiterverwendet wird.
* Auf der Benutzeroberfläche sieht der Nutzer das heruntergeladene **Preview** und kann sich entscheiden ob er dieses weiterverwenden will oder ein anderes Werk auswählt.

<big><big>Suchleiste</big></big>

In der unteren Hälfte des Tabs befindet sich eine Eingabeleiste, in die der Nutzer einen gewünschten Suchbegriff eingeben kann. 

Die Eingabe eines neuen Suchbegriffs in die Suchleiste setzt die Suche jederzeit zurück und die vorigen Ergebnisse werden gelöscht

Da Unity nativ nicht zwischen einem Fokusverlust und einer Bestätigung der Eingabe unterscheiden kann. wird im `OnEndEdit-Event` das `InputFieldScript` mit einer Methode für das jeweilige Tab aufgerufen, das überprüft ob am Ende der Eingabe diese z.B. durch das Drücken von Enter bestätigt wurde.

```csharp 
public void UnsplashEndEdit()
{
     if (TouchScreenKeyboard.isSupported && inputField.touchScreenKeyboard.status == TouchScreenKeyboard.Status.Done || Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter) || Input.GetButtonDown("Submit"))
    {
        RequestHandler.GetComponent<UnsplashRequestHandler>().InitSearchPhotos();
        RequestHandler.GetComponent<UnsplashImageLoader>().DestroyThumbs();
    }
}
```
*Methode für den Unsplash-Tab*

<big><big>RequestHandler</big></big>

Die RequestHandler Skripte sind für die Kommunitkation mit den APIs zuständig. Sie sind in der Szenenhierarchie dem Game Object *RequestHandler* als Komponenten zugeordnet. In ihnen liegt jeweils eine Methode zur Initialisierung der Suche als auch eine Methode zum Laden einer neuen Menge an Resultaten zu einem bestehenden Suchbegriff. Je nach API wird dabei unterschiedlich Verfahren.

**PolyRequestHandler**

Im `PolyRequestHandler` Skript wird die Suche in `PolyRequestHandler.InitSearchObjects` gestartet. Das **Suchfeld** wird ausgelesen und wenn dort eine Eingabe lesbar ist, wird ein ``PolyToolkit.PolyListAssetsRequest`` erstellt. Dieses erhält neben dem Suchbegriff noch die erwünschte Menge an Ergebnissen. Die Anzahl der Ergebnisse stimmt aber nicht immer mit diesem überein, häufig werden etwas mehr oder weniger Assets von der API an di Applikation gesendet. Zuletzt wird der ``PolyListAssetsRequest`` zusammen mit der Methode `PolyRequestHandler.SearchRequestCallback` der Methode ``PolyApi.ListAssets`` übergeben. 

Das Ergebnis der Anfrage wird in der Methode ``SearchRequestCallback`` abgespeichert und das Laden der Thumbnails im `PolyImageLoader` gestartet.

```csharp
public void InitSearchObjects()
{
    ... 
    PolyListAssetsRequest request = new PolyListAssetsRequest();
    request.keywords = Suchfeld.GetComponent<Text>().text;
    request.pageSize = 24;
    PolyApi.ListAssets(request, SearchRequestCallback);
}

private void SearchRequestCallback(PolyStatusOr<PolyListAssetsResult> result)
{
    ...
    lastresult = result.Value;
    ImageLoader.UpdateThumbsPoly();
    loading = false;
}
```
Soll zu einer aktuellen Suche weitere Ergebnisse geladen werden, wird dazu die Methode `PolyRequestHandler.LoadNextPage` genutzt. Das Feld `loading` hat die Funktion mehrfaches gleichzeitiges Laden einer weiteren Seite zu verhindern.

```csharp
public void LoadNextPage()
{
    loading = true;
    PolyListAssetsRequest request = new PolyListAssetsRequest();
    request.pageToken
    PolyApi.ListAssets(request, SearchRequestCallback);
}
```

**UnsplashRequestHandler**

Mit ``UnsplashRequestHandler.InitSearch`` wird im UnsplashRequestHandler die Suche initialisiert. Dabei wird der Suchbegriff aus dem Suchfeld gelesen und einem `UnsplashSearchUrlBuilder` übergeben. `UnsplashRequestHandler.UnsplashSearchUrlBuilder` ist eine innere Klasse deren Objekte Suchparameter aufnimmt und aus diesen mit der Methode `Build` eine korrekte URL für die gewünschte Suche generiert. 

```csharp
internal class UnsplashSearchUrlBuilder
{
    private string query = null;
    public string BaseURL { get; set; } = "https://api.unsplash.com/search/photos";
    public string Query { get => query; set => query = UnityWebRequest.EscapeURL(value); }
    
    ...
    
    public string Build()
    {
        string url = null;
        if (Query != null)
        {
            url = BaseURL + "?query=" + Query;

            if (Page != 0)
                url = url + "&page=" + Page;
            if (Per_Page != 0)
                url = url + "&per_page=" + Per_Page;
            if (Order_By != null)
                url = url + "&order_by=" + Order_By;
            if (Color != null)
                url = url + "&color=" + Color;
            if (Orientation != null)
                url = url + "&orientation=" + Orientation;
        }
        return url;
    }
}
```

Die URL wir dann in einer Koroutine an die Methode `UnsplashRequestHandler.SearchPhotos` übergeben. In dieser Routine wird der Request aus der URL und mit dem *Access Key* erstellt, an die API gesendet und dann mit ``yield return`` auf die Antwort gewartet.


``` csharp
public IEnumerator SearchPhotos(string searchurl)
{

    UnityWebRequest searchrequest = UnityWebRequest.Get(searchurl);
    searchrequest.SetRequestHeader("Authorization", "Client-ID " + acceskey);

    yield return searchrequest.SendWebRequest();

    ...

    string answerbody = searchrequest.downloadHandler.text;
    lastSearchResult = DeserializeResponse(answerbody);
    lastSearchResult.header = LinkHeader.LinksFromHeader(searchrequest.GetResponseHeader("Link"));
    imageloader.UpdatePicsUnsplash();
    loading = false;     
}
```

Nach dem Erhalten der Antwort wird der [*Response Body* deserialisiert](https://unsplash.com/documentation#search-photos) und in `UnsplashRequestHandler.lastSearchResult` gespeichert. Aus dem *Link Header* der Antwort werden dann noch die Informationen zu den Folgeseiten der Ergebnisse extrahiert und in `lastSearchResult.header` eingefügt. Daraufhin wird die Methode `UpdatePicsUnsplash` in UnsplashImageLoader ausgeführt.

`lastSearchResult` ist ein Objekt der Klasse `SearchResults`, welches das Ergebnis der letzten erhaltenen, erfolgreichen Antwort von Unsplash repräsentiert. Das Skript ``SearchResults.cs`` besteht dazu aus mehreren ineinander geschachtelten Klassen, die in der Klasse `SearchResults` ihre Wurzel besitzen.



Die Deserialisierung der Antworten wird in der Methode `UnsplashRequestHandler.DeserializeResponse` mit Hilfe von [Json.Net for Unity](https://github.com/jilleJr/Newtonsoft.Json-for-Unity) ausgeführt.

Zum Laden der nächsten Seite wird in ``UnsplashRequestHandler.LoadNextPage`` die Adresse der nächsten Seite aus ``lastSearchResult`` entnommen und dann direkt ``SearchPhotos`` übergeben. 

<big><big>ImageLoader</big></big>

Die ImageLoader sind dazu da, die Listen mit den letzten erhaltenen Ergebnissen nach den einzelnen Werken aufzuteilen und diese als klickbare Thumbnails auf die Benutzeroberfläche zu bringen. Die Skripts `PolyImageLoader` und `UnsplashImageLoader` gleichen sich dabei sehr stark im Aufbau, daher werden im Folgenden Codeausschnitte nur aus dem Skript `UnsplashImageLoader` gezeigt. Sie sind beide dem RequestHandler in der Szenenhierarchie zugeordnet.

Wird durch den jeweiligen RequestHandler die `UpdateThumbs`-Methode aufgerufen, so wird die Repräsentation des letzten Suchergebnis (Unsplash = ``SearchResult`` / Poly = ``PolyListAsstesResult``) durchlaufen und jedes Werk einer Instanz des Prefabs Thumbnail als ``SingleResult`` zugewießen. Ein ``SingleResult`` ist eine Klasse, die die Information zu einem Werk genau in dem Thumbnail speichert, das es zeigt. Dadurch wird verhindert, dass die Informationen zu dem Werk im lastResult verloren gehen, wenn beim Laden einer weiteren Seite das letzte Resultat überschrieben wird. 

Dann wird die Methode ``DownloadThumbnails`` mit der URL des Thumbnails aufgerufen. Bei Polyobjekten muss zuvor die Adresse des Thumbnails abgefragt werden, da diese nicht direkt mitgeliefert wird.

```csharp
internal void UpdateThumbsUnsplash()
    {
    SearchResults sr = UnsplashRequestHandler.GetComponent<UnsplashRequestHandler>().lastSearchResult;
    foreach (Photo ph in sr.results)
    {
        GameObject obj = Instantiate(ThumbPrefab);
        obj.GetComponent<SingleUnsplashResult>().photo = ph;
        StartCoroutine(DownloadThumbnails(ph.urls.thumb, obj));
    }
}
```

``DownloadThumbnails`` lädt daraufhin die Thumbnails herunter und legt diese als Textur der Prefabinstanz ab. Dann wird die Instanz dem Scrollview in der Benutzeroberfläche hinzugefügt und ist nun für den Nutzer sichtbar. Zuletzt wird dem Button, der in der Instanz als Komponente zu finden ist, das Event `OnButtonClickEvent` hinzugefügt.

```csharp
internal IEnumerator DownloadThumbnails(string MediaUrl, GameObject obj)
{
    ...
    obj.GetComponentInChildren<RawImage>().texture = ((DownloadHandlerTexture)request.downloadHandler).texture;
    Texture texture = obj.GetComponentInChildren<RawImage>().texture;
 
    ...

    obj.GetComponent<SingleUnsplashResult>().thumb = texture;

    UnityEngine.Events.UnityAction call = delegate { OnUnsplashButtonClick(obj.GetComponent<SingleUnsplashResult>()); };
    obj.GetComponent<Button>().onClick.AddListener(call);
}
```

 Das `OnButtonClickEvent` sorgt dafür, dass beim Klicken auf den Knopf die Preview zu dem Werk gezeigt wird und die Suchleiste verborgen wird.

``` csharp
private void OnUnsplashButtonClick(SingleUnsplashResult singleUnsplashResult)
{
    Preview.SetActive(true);
    Preview.GetComponentInChildren<PreviewImageScript>().result = singleUnsplashResult;
    StartCoroutine(Preview.GetComponentInChildren<PreviewImageScript>().LoadPicture());
    if (SearchBar.activeSelf)
    {
        SearchBar.SetActive(false);
    }
}
```

Weiterhin enthalten die ImageLoader eine ``DestroyThumbs`` Methode die alle angezeigten Thumbnails löscht.

<img src="..\docs\Thomas\ScreenshotThumbnails.jpg" title="Ansicht der Thumbnails" width="80%"/>

Das Laden der nächsten Seiten im ``ScrollbarScript``, das der Scrollbar der Scrollview zugeordnet ist. Erreicht die Scrollbar das letzte Prozent der scrollbaren Fläche dann löst es die Methode ``ScrollbarScript.MoreUnsplashResults`` aus. Der `if`- Block verhindert, dass die Methode mehr als einmal ausgeführt wird und zu viele weitere Seiten gleichzeitig geladen werden.
    
```csharp
public void MoreUnsplashResults()
{
    if ((Time.time - TimeOfLastLoad) < 0.5 || (urh.loading))
        return;
    Scrollbar sb = this.GetComponent<Scrollbar>();
    if (sb.value < 0.01f)
        urh.LoadNextPage();
    TimeOfLastLoad = Time.time;
} 
```

<big><big>Preview</big></big>

In der Preview wird ein ausgewähltes Werk dann zum ersten Mal in seiner vollen Größe angezeigt. Je nachdem ob man ein Poly- oder Unsplash-Werk auswählt, ist der Aufbau der Preview etwas verschieden. In der Mitte des Bildschirms ist die Preview zum Werk zu sehen. Bei beiden wird die Suchleiste ausgeblendet und an der Seite der Preview wird eine Attribution zum Werk angezeigt.

Weiterhin werden unten in der Oberfläche zwei Buttons angezeigt. Der Linke ermöglicht die Rückkehr zu der Auswahl aus allen Werken und der Suchleiste und der Rechte übernimmt das angezeigte Werk in die jeweilige nächste Szene.


**Unsplash**

Das Laden des Previews wird bei Unsplash durch das ``PreviewImageScript`` durchgeführt. Wird in der Benutzeroberfläche auf ein Thumbnail geklickt so wird im `OnButtonClickEvent` die Methode `PreviewImageScript.LoadPicture` ausgelöst. Diese lädt das Bild, in der Form herunter in der es dann in den nächsten Szenen verwendet wird und projiziert  dieses auf das für den Nutzer nun sichtbare PreviewImage der GUI. 


```csharp
public IEnumerator LoadPicture()
{
    UnityWebRequest request = UnityWebRequestTexture.GetTexture(result.photo.urls.regular);
    ...
    previewImmage.GetComponent<RawImage>().texture = ((DownloadHandlerTexture)request.downloadHandler).texture;
    AdjustImageSize(((DownloadHandlerTexture)request.downloadHandler).texture);
    result.FullImmage = previewImmage.GetComponent<RawImage>().texture;
    transform.GetChild(0).gameObject.GetComponent<Text>().text = result.GetAttributons();
}
```
<img src="..\docs\Thomas\ScreenshotPreview.jpg" title="Preview" width="80%"/>

Sollte es zu einem Fehler beim Herunterladen kommen, dann wird eine Fehlermeldung aktiviert, welche zurück zu den Thumbnails führt.

<img src="..\docs\Thomas\Fehlermeldung.jpg" title="Fehlermeldung" width="80%"/>

**Poly**

Das Script, das die Preview der 3d-Objekte bearbeitet, ist das `PreviewPositionScript`. Wird dessen Methode `LoadObject` aufgerufen so wird, das Objekt von Poly mit Hilfe von `PolyApi.Import` heruntergeladen. Der dazugehörige `ImportAssetCallback` platziert das heruntergeladene Objekt dann in der Szene als Kindobject des GameObjects `PreviewPosition`. `PreviewPosition` ist einer zweiten Kamera untergeordnet die ihre Aufnahme auf eine RenderTexture im PolyTab projeziert. Das bewerkstelligt, dass die Ansicht auf das Vorschauobjekt. im Vordergrund der GUI sichtbar ist und nicht hinter allen GUI-Elementen versteckt bleibt.  

<img src="..\docs\Thomas\ScreenshotObjekt.jpg" title="Preview" width="80%"/>

Auch hier wird ein potentieller Fehler beim Herunterladen mit einer Fehlermeldung abgefangen.

### Rahmenauswahl 
Von: <a href="https://jira.student.fiw.fhws.de:8443/secure/ViewProfile.jspa?name=k45449">Daniel Stöcklein</a>   

Nach Auswahl eines Bildes, gelangt der Nutzer in die Rahmenauswahl. Es stehen 3 Rahmen zur Auswahl zur Verfügung und können per Swipe-Geste durchgeblättert werden. Das ausgewählte Bild kann jederzeit durch touch auf den Zurück-Pfeil nachträglich geändert werden. 

<img src="..\docs\Daniel\frameSelection.gif" width="65%"/>

<big><big>Einblick in die technische Funktionsweise</big></big>

Bei den angezeigten Rahmen handelt es sich um bereits instanziierte ``GameObjects``, die innerhalb einer Szene beliebig verändert und neu instanziiert werden können.

Da allerdings bei einem Szenenwechsel alle instanziierten ``GameObjects`` zerstört (siehe [Szenenwechsel](#szenenwechsel)) und die hinterlegten Informationen verloren gehen, greift die App auf eine Kombination aus <a href="http://wiki.unity3d.com/index.php/Singleton">Singleton Pattern</a> und <a href="https://docs.unity3d.com/Manual/Prefabs.html">Prefabs</a> zurück. **Hinweis:** Auf die Verwendung von <a href="https://unity.com/de/how-to/architect-game-code-scriptable-objects">Scriptable Objects</a> haben wir verzichtet, da die App vordefinierte ``Prefabs`` verwenden soll. 

**Prefabs**

  Die Rahmen-Prefabs sind wie folgt aufgebaut: 

```
  Frame
    └── Picture   (beinhaltet das Bild)
    └── Selected  (visualisiert den Rahmen, wenn diese ausgewählt wurde)
    └── Height    (zeigt die Höhe des Rahmens an)
    └── Width     (zeigt die Breite des Rahmens an)
```

``Prefabs`` sind eine Art Container (bzw. Bauplan) für Informationen eines ``GameObjects``, welche Szenenübergreifend verfügbar sind und beliebig oft neu innerhalb einer Szene instanziiert werden können. Allerdings können ``Prefabs`` während der Laufzeit nicht verändert werden, deswegen ist es zusätzlich notwendig, Informationen über das ausgewählte Bild und den ausgewählten Bilderrahmen Szenenübergreifend zu speichern.

**Das Singleton-Pattern**

Mithilfe des ``Singleton Patterns`` wird genau das sichergestellt; indem für eine Klasse immer nur eine einzige global zugängliche Instanz verfügbar ist (ähnlich einer statischen Klasse). Das ``Singleton Pattern`` ist in der Klasse ``PersistentManager`` implementiert:

<img src="..\docs\Daniel\frameSelectionClassDiagramm3.png" width="30%" align="right"/>

```csharp
public class PersistentManager : MonoBehaviour
{
    public static PersistentManager Instance { get; private set; }

    public string frameName; //stores the name of the selected frame as type string
    public Material picture; //stores the picture as type material

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
}
```
Die Variable ``frameName`` beinhaltet den Namen des ausgewählten Bilderrahmens, während ``picture`` das zuvor ausgewählte Bild beinhaltet. Beide Informationen bleiben Szenenübergreifend verfügbar. Die Klasse beinhaltet eine statische Variable ``Instance`` des Typs ``PersistentManager`` und ist somit eine Instanz seiner eigenen Klasse. 

Die Methode ``Awake`` wird von der App ausgeführt sobald das ``GameObject`` welches das Skript beinhaltet geladen wurde. Auf diese Weise können Initialisierung innerhalb des ``PersistentManager`` noch bevor der Methode ``Start``, welche erst bei Aktivierung des ``GameObjects`` ausgeführt wird, stattfinden. Bei erstmaliger Ausführung des Skripts, ist ``Instance`` leer (null) und wird deswegen eine Instanz des Typs ``PersistentManager`` zugewiesen. Anschließend wird der App mittgeteilt, dass das ``GameObject`` welches dieses Skript beinhaltet, bei Szenenwechsel nicht zerstört werden darf. Falls ``Instance`` nicht leer ist, bedeutet das, dass ein Szenenwechsel stattgefunden hat und die App beim Laden der neuen Szene versucht eine Instanz des Objektes, welches diese Klasse beinhaltet, zu erstellen. Dieses ``GameObject`` kann somit zerstört werden. Das verhindert, dass bei Szenenwechsel immer neue ``PersistentManager`` Objekte erstellt werden (Singleton). 

Alle Szenen innerhalb der App haben somit Zugriff auf die Informationen ``frameName`` und ``picture`` und können diese verwenden, um die benötigten Ressourcen zu laden. So ist beispielsweise die nachfolgende AR-Szene mithilfe der Befehle 

```csharp 
objectToSpawn = Resources.Load("Prefabs/" + PersistentManager.Instance.frameName) as GameObject;
child.gameObject.GetComponent<Renderer>().material = PersistentManager.Instance.picture;
```

in der Lage, den zuvor ausgewählten Bilderrahmen und das ausgewählte Bild zu laden.

<big><big>Die Anzeige der Bilderrahmen</big></big>

<img src="..\docs\Daniel\frameSelectionClassDiagramm1.png" width="70%"/>

Grafik: <a href="https://jira.student.fiw.fhws.de:8443/secure/ViewProfile.jspa?name=k45449">Daniel Stöcklein</a> 

Die App verwendet das kostenlose Asset <a href="https://assetstore.unity.com/packages/tools/gui/swipe-menu-45977">Swipe Menu</a> aus dem Unity <a href="https://assetstore.unity.com/">Asset-Store</a>, welches eine Grundstruktur eines Auswahlmenüs inkl. Swipe-Funktion zur Verfügung stellt. Für die Darstellung der Rahmen ist das Skript ```Menu``` zuständig. 

Zunächst wird in der ```Awake``` Methode die Distanz sowie die Anzeige der Radio-Buttons initialisiert. 

```csharp
    void Awake()
    {
        ...
        //Daniel Stöcklein
        startingRadioItem = startingMenuItem;
        radios[startingRadioItem - 1].transform.GetChild(0).gameObject.SetActive(true);
        _currentMenuPosition = ((1) * distanceBetweenMenus) * startingMenuItem;
        ...
    }
```
Anschließend wird die ```Start``` Methode ausgeführt, welche die benötigten Ressourcen lädt und das zuvor ausgewählte Bild in die Bilderrahmen platziert.

```csharp
    void Start()
    {
        LoadResources();
        PlacePictures();
    }
```

Dazu wird das Standard-Material geladen, welches das zuvor ausgewählte Bild beinhaltet, und dem PersistentManager übergeben.

```csharp
    private void LoadResources()
    {
        picture = Resources.Load("m", typeof(Material)) as Material;
        PersistentManager.Instance.picture = picture;
    }
```

Jeder Bilderrahmen ist als Typ ```MenuItem``` definiert und wird im Datenfeld ```menuItems[]``` als einfaches Array aufgelistet. Die Platzierung der Bilder erfolgt in einer Schleife. Es wird für jedes ```MenuItem``` geprüft, ob das notwendige ```GameObject``` ```Picture``` vorhanden ist. Falls dem so ist, wird das geladene Bild platziert.



```csharp
    private void PlacePictures()
    {
        foreach (MenuItem g in menuItems)
        {
            GameObject child = TagSearcher.FindObjectsWithTag(g.transform, "Picture");
            if (child != null)
            {
                child.gameObject.GetComponent<Renderer>().material = picture;
            }
            else
            ...
        }
    }
```

<big><big>Die Auswahl der Bilderrahmen</big></big>


Alle angezeigten Bilderrahmen reagieren wie Buttons auf Tap-Gesten. Dazu ist im Skript ```MenuItem``` für jeden Bilderrahmen ein OnClick Event hinterlegt, welches ausgelöst wird, sobald auf einen Bilderrahmen per Touch geklickt wird.

```csharp
    public class MenuItem : MonoBehaviour
    {
        /// <summary>
        /// The behaviour to be invoked when the menu item is selected.
        /// </summary>
        public Button.ButtonClickedEvent OnClick;
        ...
    }
```

Sobald ein OnClick-Event registriert wurde, wird im Skript ```FrameSelector``` der Name des ausgewählten Bilderrahmens als string dem ```PersistentManager``` übergeben.

```csharp
    public class FrameSelector : MonoBehaviour
    {
        public void Select (string name)
        {
            PersistentManager.Instance.frameName = name;
        }
    }
```

Der gewünschte Bilderrahmen ist nun erfolgreich ausgewählt und kann in der nachfolgenden AR-Szene augmentiert werden. Die AR-Szene wird geladen.

<big><big>Die Swipe-Funktion</big></big>

<img src="..\docs\Daniel\frameSelectionClassDiagramm1.png" width="70%"/>

Grafik: <a href="https://jira.student.fiw.fhws.de:8443/secure/ViewProfile.jspa?name=k45449">Daniel Stöcklein</a> 

Die Klasse ```SwipeHandler``` sorgt dafür, dass ausschließlich Swipe-Gesten zum Blättern der Bilderrahmen funktionieren. Desweitere wird durch das Attribut ```handleFlicks``` bestimmt, ob auch Flick-Gesten registriert werden sollen, welche in Swipe-Gesten umgewandelt werden. Der Bilderrahmen, welcher am nächsten zum Mittelpunkt des Displays steht, wird zentriert.

Die Klasse ```TouchHandler``` registriert Tap-Gesten und reagiert sobald ein Bilderrahmen geklickt wurde. Auschließlich zentrierte Bilderrahmen können angeklickt - und damit das OnClick-Event ausgeführt - werden. Ein kurzer Ausschnit aus der Methode ```CheckTouch``` welche das beschriebene Verhalten realisiert:

```csharp
    private void CheckTouch(Vector3 screenPoint)
    {
        if (hit.collider != null && hit.collider.gameObject.CompareTag("MenuItem"))
        {
            var item = hit.collider.GetComponent<MenuItem>();
            if (Menu.instance.MenuCentred(item))
            ...
            item.OnClick.Invoke();
        }
    ...
    }
```

### Flächenerkennung 

Von: <a href="https://jira.student.fiw.fhws.de:8443/secure/ViewProfile.jspa?name=k41016">Simon Weickert</a> 

Zu den Trackables gehören alle möglichen AR generierte Elemente: </br>
verschiedene Arten von `Planes`, `Reference Points`, `Anchors`

Obwohl die Erkennung der horizontalen und vertikalen Flächen von AR Foundation unterstützt werden, ist für die AR Szenen nur die horizontale Erkennung eingeschaltet.
Der Grund dafür ist zum Einen, dass viele Wandflächen sehr einfarbig und monoton sind, wodurch die Erkennung von Featurepoints nur eingeschränkt möglich ist und zum Anderen, dass die gleichzeiige Nutzung beider Arten zu instabilerem Verhalten der platzierten Objekte führte. Außerdem macht es die Skripte zur Platzierung und Manipulation der Objekte komplexer.

<big><big>Vertikale Wände</big></big>
 
Für die Platzierung von Bildern, wird zunächst eine vertikale Wand benötigt.
Diese wird im `Placementindicator` Skript erstellt.
Sobald der User mit dem Handy auf eine erkannte horizontale AR Fläche zeigt, wird auf dieser ein `Indikator` angezeigt. Über diesem wird auch bereits eine Vorschau des Bildes angezeigt.



Position und Orientierung des Indikators wird mit jedem Frame aktualisiert. Bestimmt wird die Position durch deinen Raycast, welcher mittig vom Handy ausgeht. Sobald dieser eine Fläche vom Typ `Trackables.PlaneEstimated` trifft, werden die Daten des Schnittpunktes an das Parent-Objekt weitergegeben. 
Sobald der User dann auf die Fläche klickt, geschehen folgende Ereignisse:
* Der Indikator wird festgesetzt
* Eine unsichtbare vertikale Wand wird aktiviert und steht senkrecht zum Boden
* Ein animierter Pfeil erscheint über dem Marker und zeigt zum Bild
* Das Bild wird opak

Folgedessen hat der Nutzer die Möglichkeit mit der erstellten Wand zu interagieren, indem er weitere Bilder, mit dem *"Neues Bild"* Button platziert, oder ein erstelltes Bild entlang der Wand verschiebt.
Der Indikator, die vertikale Wand und ein Holderelement für die Position des ersten Bildes wird in einem Gameobjekt, dem `Marker Holder` gespeichert. Auf diesem Objekt befindet sich auch das Placementindicator Skript. Neue Marker Holder Elemente werden erstellt, sobald der User das erste Mal in die AR Szene gelangt, oder den Button *"Neuer Marker"* betätigt.
Die Marker werden nur erzeugt, wenn im `MarkerManager` Skript die `newMarkerAllowed` Eigenschaft auf *wahr* gesetzt ist.

### Bild Platzierung
Von: <a href="https://jira.student.fiw.fhws.de:8443/secure/ViewProfile.jspa?name=k41016">Simon Weickert</a> 

<img src="..\docs\Simon\AR_Szene_Bilder.png" width=75%> 

Die Platzierung der Bilder wird in dem `ObjectSpawner` Skript geregelt.
Folgender Codeblock wird ausgeführt, sobald das Event für einen `Raycasthit` auf ein 3D-Objekt mit `Collider` erkannt wurde. Auf das [EventSystem](#EventSystem) wird im späteren Kapitel noch genauer eingegangen.
```csharp
    //Event: Touch && Raycasthit auf 3D Element
    private void Action_OnTouchAndRaycastHitPhysicsObject(RaycastHit hitObject)
    {
        if (allowedToPlacePicture == true && 
            hitObject.transform.gameObject.name.Contains("VerticalPlane"))
        {
            placePicture(hitObject.point, hitObject.transform.rotation);

            if (placedObject.transform.parent == null)
                placedObject.transform.SetParent(hitObject.transform, true);
            selectable.Select(placedObject);
            allowedToPlacePicture = false;
        }
    }
```
Wenn der `Raycasthit` auf einer Vertikale Plane liegt und gerade ein neues Bild platziert werden darf, dann wird in der Methode `placePicture` das Bild instanziiert und die Maße des Bildes werden initialisiert. Folglich wird die Wand als `Parent` des Bildes gesetzt.
Und schließlich wird das Bild noch initial selektiert (Siehe [Selektion](#Selektion)).

<img src="..\docs\Simon\Platzierung_Bild.gif" width=55%> 

### Bild Manipulation   
Von: <a href="https://jira.student.fiw.fhws.de:8443/secure/ViewProfile.jspa?name=k45449">Daniel Stöcklein</a>   

Für die Manipulation an den augmentierten Bildern sind die Klassen `ObjectManipulator` und `Selectable` zuständig.

<img src="..\docs\Daniel\objectManipulatorClassDiagramm.png" width="60%"/> 

Grafik: <a href="https://jira.student.fiw.fhws.de:8443/secure/ViewProfile.jspa?name=k45449">Daniel Stöcklein</a> 

<big><big>Selektion der Bilder</big></big>

Voraussetzung für Manipulationen am Bild ist, dass es zunächst selektiert bzw. ausgewählt wird. Der User kann dazu Augmentierte Bilder per Tap auf das Display selektieren. Eine entsprechende Visualisierung zeigt an, welches Bild gerade selektiert ist (es kann immer nur 1 Bild gleichzeitig selektiert sein). Die Klasse `Objectmanipulator` registriert sämtliche Touches und reagiert dementsprechend. Für die Selektierung ist die Klasse `Selectable` zuständig - diese Klasse wird jedem Bilderrahmen in Unity ( `Prefab` ) als Komponente zugewiesen. Dadurch weiß die App, dass dieses Objekt selektierbar ist. Das folgende Aktivitätsdiagramm gibt einen groben Überblick über die entsprechende Vorgehensweise.

<img src="..\docs\Daniel\Aktivit%C3%A4tsdiagramm_Selektion_DanielST.png" width="70%"/> 

Grafik: <a href="https://jira.student.fiw.fhws.de:8443/secure/ViewProfile.jspa?name=k45449">Daniel Stöcklein</a> 

Die Methode ```Update``` wird jede ```Frames per Second``` ausgeführt. Sobald mindestens ein Finger-Touch registriert wurde, wird geprüft ob es sich um eine Tap-Geste handelt ( `TouchPhase.Began` ). Ist dies der Fall, wird ein Raycast ausgeführt. Ein Raycast ist im Wesentlichen ein Strahl, der von einer Position im 3D- oder 2D-Raum ausgesendet wird, sich in eine bestimmte Richtung bewegt und Kollisionen mit ```GameObjects``` registriert. Des Weiteren wird in einer boolschen Variable gespeichert, ob der Touch auf einem UI-Element, wie z.B. Buttons, erfolgte.

``` csharp
    void Update()
    {
        if (Input.touchCount > 0) //Display touched?
        {
            touch = Input.touches[0]; //Save the first Touch position
            touches = Input.touches; //Save all Touches
            uiTouched = uiController.IsTouchOverUIObject(touch.position); //touched on UI-Element?
            switch (touch.phase) //Which TouchPhase?
            {
                case TouchPhase.Began: 
                    DoRaycast(); //perform raycast
                    break;
        ...
    }
```

Wenn der Raycast auf ein `GameObject` stattfand, wird zunächst abgefragt ob ein Bild berührt wurde. Anschließend wird der erwähnte boolsche Wert ```uiTouched``` abgefragt - wenn dieser true ist, dann wurde ein UI-Element berührt und die weiteren Befehle der Methode werden nicht ausgeführt. Wenn ein Bild berührt wurde und ein anderes bereits selektiert ist, wird dieses deselektiert. Ansonsten wird das berührte Bild selektiert (aber nur wenn kein UI-Element berührt wurde).

``` csharp
    void DoRaycast()
    {
        Ray ray = cam.ScreenPointToRay(touch.position);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit))
        {
            //Check if something got hit and if its a pictureframe
            if (hit.collider.gameObject != null && hit.transform.name.Contains("frame1"))
            {
                if (!uiTouched && curSelected != null) //Another pictureframe already selected -> deselect it
                {
                    selectable.Deselect(curSelected);
                    newPictureButton.SetActive(true);
                    destroyPictureButton.SetActive(false);
                    curSelected = null;
                }
                if (!uiTouched) //No pictureframe selected & no UI-Element touched -> select it
                {
                    curSelected = hit.collider.gameObject;                    
                    selectable.Select(curSelected);
                    newPictureButton.SetActive(false);
                    destroyPictureButton.SetActive(true);
                ...
            }
        ...
    }
```

Für die Visualisierung der selektierten Bilder und zur Überprüfung, ob ein Bild überhaupt selektiert werden kann, ist die Klasse `Selectable` zuständig. Hier wird geprüft, ob das Bild das `Selectable` Script als Komponente hält. Ist dies der Fall wird außerdem geprüft, ob das Bild bereits selektiert ist. Wenn nicht, wird das Bild selektiert und entsprechend für den Nutzer visualisiert. Die Methode `Deselect()` geht im Prinzip umgekehrt vor und hebt die Selektierung auf. Wenn ein Bild selektiert ist, wird außerdem ein Löschen-Button angezeigt, siehe [Löschen der Bilderrahmen](#löschen-der-bilderrahmen).


``` csharp
    public void Select(GameObject obj)
    {
        if(obj == null) return;

        if (IsSelectable(obj)) //Is object selectable?
        {
            if (IsAlreadySelected() == false) //Is object already selected?
            {
                selectable.SetActive(true); //visualize selection
            }
        ...
    }
```

<big><big>Dragging & Scaling der Bilder</big></big>

**Hinweis:** Im Gegensatz zu den Skulpturen, wird bei der Manipulation der Bilder auf das ```LeanTouch``` Asset verzichtet.

Die App unterstützt das Bewegen von **selektierten** Bildern via Drag-Gesten und das Skalieren via Pinch-Gesten. Hierfür ist die `Objectmanipulator` Klasse zuständig. Sie registriert welche Touch-Geste vom Nutzer durchgeführt wurde. 

Für das Bewegen der Bilder wird auf die Phase `TouchPhase.Moved` abgefragt, was bedeutet, dass der Nutzer auf das Display drückt und den Finger über das Display bewegt. Das Bild bewegt sich stehts relativ zur Wand, um zu verhindern, dass es sich dahinter verschieben lässt. 

```csharp
    void DragSelected()
    {
        //convert from pixel to viewportpoints, to get app running on all resolutions
        Vector3 curPos = cam.ScreenToViewportPoint(touch.position);
        Vector3 lastPos = cam.ScreenToViewportPoint(touch.position - touch.deltaPosition);
        Vector3 touchDir = curPos - lastPos;
        touchDir.z = 0; //ignore z-axis

        //Drag along vertical plane
        curSelected.transform.position += verticalPlane.transform.TransformDirection(touchDir);
    }
```

Für die Skalierung muss lediglich bestimmt werden, ob genau zwei Finger gleichzeitig das Display berühren und ob ein Bild selektiert wurde. Die Methode `ScaleSelected()` errechnet wie weit die Position der beiden Finger auseinander liegen und skaliert das Bild mit einem dementsprechenden Faktor. Außerdem wird auf maximale und minimale Skalierung geprüft. Ausschnitt aus der Methode:

```csharp
    void ScaleSelected()
    {
        Touch t1 = touches[0]; //first finger touch
        Touch t2 = touches[1]; //second finger touch
        ...
        float currentFingersDistance = Vector2.Distance(t1.position, t2.position); //calculate distance between both fingers
        float scaleFactor = currentFingersDistance / initialFingersDistance; //calculate factor
        Vector3 newScale = initialScale * scaleFactor;
        if (newScale.x > minScale && newScale.y > minScale && newScale.x < maxScale && newScale.y < maxScale)
        {
            curSelected.transform.localScale = newScale;
        }
        ...
    }
```

<big><big>Löschen der Bilder</big></big>

Wenn ein Bild selektiert ist, wird automatisch ein Löschen-Button innerhalb der AR-Szene angezeigt. Durch Touch auf den Löschen-Button wird das ausgewählte Bild zerstört:

```csharp
    public void DestroyPicture()
    {
        Destroy(curSelected);
        newPictureButton.SetActive(true);
        destroyPictureButton.SetActive(false);
    }
```

Anschließend wird der Löschen-Button deaktiviert und der Button zum Setzen eines neuen Bilder aktiviert.


### Skulptur Platzierung

Von: <a href="https://jira.student.fiw.fhws.de:8443/secure/ViewProfile.jspa?name=k41016">Simon Weickert</a>

<img src="..\docs\Simon\AR_Szene_Skulpturen.png" width=75%> 

Die Skulpturen haben ihre eigene Szene und die Platzierung erfolgt hierbei über das `SculpturePlacer` Skript.
Sobald vom User ein 3D-Objekt aus der Polydatenbank ausgewählt wurde, gelangt er in die `AR Sculpture` Szene. Dort wird zunächst in der `Start()`-Methode der übertragene `Objectcarrier` abgespeichert. Dieses Trägerobjekt erhält alle zukünftigen Polyobjekte als Kinderelemente. Weiterhin wird beim Start das erste Polyobjekt initialisiert durch die Methode `initPolyObject()`. 

```csharp
public void initPolyObject()
{
    int loadedPolys = objectCarrier.transform.childCount;
    if (loadedPolys > 0)
    {
        polys.Add(new Poly());
        newest = polys.Count - 1;
        polys[newest].sculpture = Instantiate(objectCarrier.transform.GetChild(loadedPolys - 1).gameObject, Vector3.zero, new Quaternion(0, 0, 0, 0));
        polys[newest].sculpture.name = "sculpture " + newest;

        adjustSizeOfPolyObject();
        adjustPositionOfPolyObject();
        changeToTransparent(true);

        polys[newest].marker = Instantiate(hitMarker, Vector3.zero, new Quaternion(0, 0, 0, 0));

        polys[newest].marker.SetActive(false);
        polys[newest].sculpture.transform.SetParent(polys[newest].marker.transform);

        PrepareForLeanTouch();

        alreadyHitHorizontalPlane = false;
        polyInitiated = true;
    }
    else
    {
        Debug.LogError("Object Carrier has no child!");
    }
}
```

In einer Liste `polys` sind alle Polyelemente in einer eigenen `Poly Klasse` abgespeichert. Diese Klasse beinhaltet das Gameobjekt, den benutzten Marker, die Materialien und ein Boolean, welcher aussagt, ob das Objekt schon platziert wurde.
In dem Skript wird die Skupltur instanziiert, mit einer ID versehen und dann in seiner Größe skaliert (siehe [Initialskalierung](Initialskalierung)) und die Position des Objektes muss auch relativ zu seinem Pivotelement angepasst werden (Siehe [Initialpositionierung](Initialpositionierung)). Außerdem soll die Skulptur zuerst halbtransparent angezeigt werden und erst beim Platzieren opak werden (Siehe [Objekttransparenz](Objekttransparenz)).
Zum Schluss bekommt die Skulptur noch ein Marker auf der Position des Pivotelementes und damit sich die Objekte transformieren lassen, müssen noch Vorbereitungen für Lean Touch Asset ausgeführt werden (Siehe [Skulpturen Manipulation](#Skulpturen-Manipulation)).

<big><big>Initialskalierung</big></big>


Um die übergebene Skulptur auf eine passende Größe zu bringen, wird die größte Seitenlänge ermittelt und dann ein `scalefactor`berechnet. Das Ziel ist hierbei, dass das Objekt in seiner längsten Seitenlänge einen halben Meter misst.
```csharp
    public void adjustSizeOfPolyObject()
    {
        MeshRenderer[] mR_arr = polys[newest].sculpture.GetComponentsInChildren<MeshRenderer>();
        Vector3 biggestSize = Vector3.zero;
        float biggest = 0;
        foreach (MeshRenderer mR in mR_arr)
        {
            biggestSize = Vector3.Max(biggestSize, mR.bounds.size);
        }
        biggest = Mathf.Max(biggestSize.x, biggestSize.y);
        biggest = Mathf.Max(biggest, biggestSize.z);

        float scaleFactor = 0.5f / biggest;

        polys[newest].sculpture.transform.localScale *= scaleFactor;
    }
```

<big><big>Initialpositionierung</big></big>

Damit die Skulptur auch wirklich auf dem Boden steht und nicht im Boden hängt, muss diese relativ zum `Pivotelement` verschoben werden. Dafür wird der niedrigste Punkt aus der `Bounding Box` entnommen und dann der Skulptur übergeben.
```csharp
    public void adjustPositionOfPolyObject()
    {
        MeshRenderer[] mR_arr = polys[newest].sculpture.GetComponentsInChildren<MeshRenderer>();
        float yMin = 1;
        foreach (MeshRenderer mR in mR_arr)
        {
            if (yMin > mR.bounds.min.y)
                yMin = mR.bounds.min.y;
        }
        polys[newest].sculpture.transform.position = new Vector3(Vector3.zero.x, -yMin, Vector3.zero.z);
    }
```

<big><big>Objekttransparenz</big></big>

Um das Objekt transparent zu machen, werden alle Materialien ausgetauscht. Dafür werden die Originalmaterialien zuerst in einer Liste abgespeichert und dann ein vorher bestimmtes transparentes Material hinzugefügt. Um diesen Vorgang rückgängig zu machen, wird das abgespeicherte Material wieder den einzelnen Objekten zurückgegeben.
```csharp
    public void changeToTransparent(bool transparent)
    {
        if(transparent)
            MeshRenderer[] meshR_Sculpture = polys[newest].sculpture.transform.GetComponentsInChildren<MeshRenderer>();

        for (int i = 0; i < meshR_Sculpture.Length; i++)
        {
            if (transparent)
            {
                polys[newest].materialsSave.Add(meshR_Sculpture[i].material);
                meshR_Sculpture[i].material = transparentMaterial;
            }
            else
            {
                meshR_Sculpture[i].material = polys[newest].materialsSave[i];
            }
        }
    }
```
Sobald der erste Raycast eine AR Plane trifft, wird die halbtransparente Skulptur und der Marker für den User sichtbar. Alle weiteren Raycasts auf die AR Planes transformieren dann nur noch Position und Rotation des Objektes.
Sobald aber der User einen Touch auf die Plane macht, hören diese Transformationen auf und das Objekt wird wieder opak gesetzt. Somit steht die Skulptur nun auf dem Boden und kann von nun an durch das Lean Touch Asset verschoben, skaliert und rotiert werde (Siehe [Skulptur Manipulation](#Skulptur-Manipulation)).

<img src="..\docs\Simon\Platzierung_Skulptur.gif" width=60%> 

### Skulptur Manipulation

Von: <a href="https://jira.student.fiw.fhws.de:8443/secure/ViewProfile.jspa?name=k41016">Simon Weickert</a> 

<big><big>Lean Touch</big></big>

Für die Bewegung, Rotation und Skalierung der Skulpturen wird das `Lean Touch` Asset verwendet.
Damit lassen sich verschiedene Touchbefehle einfach ausführen. Die Konfiguration erfolgt hierbei einfach über den Unity Editor. Damit das Asset läuft müssen drei verschiedene Skripte laufen. Dafür gibt es ein Objekt mit dem `LeanTouch` Skript, welches für die Grundeinstellungen der Toucheingabe benötigt wird. Ein weiteres Objekt beinhaltet das `LeanSelect` und das `LeanFingerTap` Skript. Mit diesen Skripten werden weitere Einstellungen getroffen.
Da der Marker zu Laufzeit als Parent der Skulptur gesetzt wird, bekommt er die folgenden Lean Touch spezifischen Komponenten:
* Lean Selectable + Lean Selectable Renderer
* Lean Drag Translate
* Lean Twist Rotate Axis
* Lean Pinch Scale

Damit sich nun auch die Skulpturen auswählen lassen, müssen ihnen zur Laufzeit noch zwei weitere Komponenten hinzugefügt werden:
* Collider (Für den Raycast)
* Lean Selectable

Schließlich werden vom Lean Selectable Script die Events OnSelect und OnDeselect so konfiguriert, dass bei einem Klick der Skulptur folgedessen der Marker auch davon beeinflusst wird.

### EventSystem

Von: <a href="https://jira.student.fiw.fhws.de:8443/secure/ViewProfile.jspa?name=k41016">Simon Weickert</a> 

<img src="..\docs\Simon\EventPublisher.png" width=80%> 

Ein eigenes `Eventsystem` wurde errichtet, um überflüssige Raycasts zu vermeiden. So werden in einem eigenen Script, dem `EventPublisher` die Events erzeugt, welche dann von anderen Skripte aufgefasst werden und ihre spezifischen Funktionen erfüllen.

Um die entscheidenden Informationen den Events mitzugeben, gibt es eigene `Delegate` Typen.
```csharp 
(1) delegate void arRaycastEventWithHits(List<ARRaycastHit> hitList);
(2) delegate void RaycastEventWithHitobject(RaycastHit hitObject);
```

Events mit Delegate Typen:
* On Touch Began (normaler EventHandler Delegate) 
* On Touch and Raycast Hit AR Plane (1)
* On Raycast Hit AR Plane(1)
* On Touch and Raycast Hit Physics Object (2)

Letztendlich sollten alle Skripte von diesem Eventsystem profitieren, jedoch kam gegen Schluss die Entscheidung, für den `SculpturePlacer` eigene Raycasts zu nutzen, da für die Skulpturen statt dem TrackableType `PlaneEstimated` der TrackableType `PlaneWithinPolygon` besser funktioniert.

<big><big>PlaneWithinPolygon VS PlaneEstimated</big></big>

Bei den Raycasts wird als Parameter angegeben, welche Arten von `Trackables` erkannt werden sollen. Während beim `PlaneWithinPolygon` nur die genauen Größen der erkannten Flächen zählen, gelten bei `PlaneEstimated` ein weitaus größerer Bereich. Die Kollision zwischen dem Raycast und dem geschätzten Flächenbereich kann sinnvoll bei der Markerplatzierung in der Bilder AR Szene sein, da der Maker präzise an eine Wand platziert werden soll. Würde die erkannte Fläche etwas krumm liegen oder nicht genau mit der Wand abschließen, dann kann der Marker nicht korrekt platziert werden. Probleme stellen auch, im Weg stehende, Gegenstände/Möbel dar.
Ein störender Effekt tritt auf, wenn es unterschiedlich hohe AR Flächen gibt und `PlaneEstimated` abgefragt wird. Dann überlappt der höherliegende Bereich die niedrigere Fläche (siehe Bild). Damit nun der Marker nicht in der Luft platziert werden kann, wird im `PlacementIndicator` wie folgt vorgegangen:
Das Skript kriegt die Eventauslösung von `OnRaycastHitARPlane` mit und bekommt die Liste mit den ARRaycastHits übergeben. Die Liste beinhaltet, dann alle Treffer vom Strahl und in der Reihenfolge, in der die Treffer geschehen. Also solange das Handy über den Planes positioniert ist, steht in der Liste immer als letztes der niedrigste Treffer drinnen. Die Position wird dann für die Visualisierung des Markers verwendet.
Bei der Platzierung der Skulpturen kann diese Vorgehensweise jedoch nicht genutzt werden, da in diesem Fall die Skulptur nicht immer auf der niedrigsten Plane stehen soll. In einer Situation wie im Bild, könnte die Skulptur dann unter dem Bett platziert werden. Die Nutzung des TrackableTypes `PlaneWithinPolygon` ist hierbei somit praktischer.


<img src="..\docs\Simon\PlaneEstimated.jpg" width=60%> 

### Side-Menu

Von: <a href="https://jira.student.fiw.fhws.de:8443/secure/ViewProfile.jspa?name=k45449">Daniel Stöcklein</a>

Innerhalb der AR-Szene können bequem über ein Side-Menu verschiedene Aktionen ausgeführt werden. Dazu verwendet die App das Asset `SimpleSideMenu` aus dem Unity-Store. 

<img src="..\docs\Daniel\sideMenu.png" width="70%"/> 

Das Side-Menu öffnet sich durch einen Touch auf das Menü-Symbol am rechten oberen Bildschirmrand. Es können Screenshots, Bilder ins Museum, Bilder geändert oder Rahmen geändert werden. Das Menü schließt mit einem Touch außerhalb des Side-Menu Bereichs.

<big><big>Screenshot</big></big>

Klasse: Screenshot  
Von: <a href="https://jira.student.fiw.fhws.de:8443/secure/ViewProfile.jspa?name=k42282">Haris Hodzic</a> 

<img src="..\docs\Haris\Screenshot_Share.png" title="Museum" width="85%"/>

Damit ein Screenshot (ungeläufiger: Bildschirmfoto) erstellt werden kann wurde eine Koroutine geschrieben. Dies ist auch notwendig, da für den Screenshot alle UI-Elemente deaktiviert werden müssen, bevor die Bildschirmfläche gerendert wird. 

Bei „yield return null“ wird dabei die Ausführung pausiert und alle Elemente der Canvas deaktiviert. Danach wird gewartet, bis die komplette Bildschirmfläche gerendert wurde. 

```csharp
yield return null; 
GameObject.Find("Canvas").GetComponent<Canvas>().enabled = false;
yield return new WaitForEndOfFrame(); 

```

Anschließend wird eine 2D-Textur der gerenderten Bildschirmfläche erstellt. Diese wird dann ins JPG-Format kodiert und im Unterordner „Screenshots“ der App gespeichert. 
Um die erstellten Screenshots auch in der Galerie-Applikation eines Smartphones anzeigen können, wurde ein Plugin verwendet, welches ein Byte-Array des ins JPG-Format kodierten Bildes übergeben bekommt und dieses in der Galerie-Applikation speichert. 

```csharp
Texture2D screenImage = new Texture2D(Screen.width, Screen.height);
screenImage.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
screenImage.Apply();

byte[] imageBytes = screenImage.EncodeToJPG();
filePath = Application.persistentDataPath + "/Screenshots/" + fileName;

File.WriteAllBytes(filePath, screenImage.EncodeToJPG());
NativeGallery.SaveImageToGallery(imageBytes, "Camera", fileName, null);
```


Ist der Vorgang erfolgreich und der Screenshot befindet sich im Speicherort, wird eine Toast-Message angezeigt. 
```csharp
if (File.Exists(filePath))
{
    Toast_Message.messageTxt("Bild gespeichert!");
}
else
{
    Toast_Message.messageTxt("Fehler aufgetreten!");
}
```

Die Toast-Message wird über die Instanziierung eines Prefabs realisiert, dem ein String angefügt wird. Das instanziierte Objekt wird dann anschließend wieder zerstört.

```csharp
public class Toast_Message : MonoBehaviour
{
    public static void messageTxt(string msg)
    {
        //Prefab MessageBox mit Bestandteilen wird aus Resource-Ordner geladen
        GameObject messagePrefab = Resources.Load("Toast_Message") as GameObject;
        GameObject containerObject = messagePrefab.gameObject.transform.GetChild(0).gameObject;
        GameObject textObject = containerObject.gameObject.transform.GetChild(0).GetChild(0).gameObject;

        //Stringvariable wird TextObjekt zugewiesen
        Text msg_text = textObject.GetComponent<Text>();
        msg_text.text = msg;

        //Objektkopie wird erstellt (Position und Rotation können bei dem Klon ggf. angepasst werden)
        GameObject clone = Instantiate(messagePrefab);

        removeMsg(clone); //löschen der Kopie
    }

    public static void removeMsg(GameObject clone)
    {
        Destroy(clone.gameObject, 3f);
    }
}

```

Danach werden alle Elemente der Canvas wieder aktiviert und es wird gewartet, bis dieser Vorgang komplett abgeschlossen ist, bevor ein neuer Screenshot erstellt werden kann.

```csharp
GameObject.Find("Canvas").GetComponent<Canvas>().enabled = true;
yield return new WaitUntil(() => isFocus);
```

<big><big>Teilen der Bilder</big></big>

Wenn ein Screenshot erstellt wurde, wird der Nutzer gefragt, ob er diesen Teilen möchte. Um den erstellten Screenshot über verschiedene Plattformen und Social-Media-Kanäle teilen zu können ist die Einbindung der nativen Teilen-Funktion von Android notwendig, was wiederum mit der Verwendung von Android Klassen einhergeht.  

Da die Fileprovider-Klasse von Android in Share.cs verwendet wird, muss ein Android Library Modul in Android Studio erstellt werden, in dem die notwendige Konfiguration festgelegt ist. 

```csharp
apply plugin: 'com.android.library'

android {
    compileSdkVersion 29
    buildToolsVersion "29.0.3"

    defaultConfig {
        minSdkVersion 24
        targetSdkVersion 29
        versionCode 1
        versionName "1.0"

        testInstrumentationRunner "androidx.test.runner.AndroidJUnitRunner"
        consumerProguardFiles 'consumer-rules.pro'
    }

    buildTypes {
        release {
            minifyEnabled false
            proguardFiles getDefaultProguardFile('proguard-android-optimize.txt'), 'proguard-rules.pro'
        }
    }
}

dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar'])
    implementation 'androidx.appcompat:appcompat:1.1.0'
}
```
Weiterhin muss Provider definiert werden. Darin werden auch URI die notwendigen Berechtigungen erteilt und externer Schreibzugriff gewährt. 

```csharp
<manifest xmlns:android="http://schemas.android.com/apk/res/android"
    package="com.harishodzic.unitysharing">
    <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE"/>
    <application>
        <provider
            android:name="androidx.core.content.FileProvider"
            android:authorities="${applicationId}.provider"
            android:exported="false"
            android:grantUriPermissions="true" >
            <meta-data
                android:name="android.support.FILE_PROVIDER_PATHS"
                android:resource="@xml/provider_paths" />
        </provider>
    </application>
</manifest>
```
Das Android Library Modul kann als Android Archive File (aar) von Unity verwendet werden (Datei: unitysharing-release.aar).

Da der FileProvider nur in der Support-Bibliothek(appcombat) von Android definiert ist, ist folgender Eintrag in der mainTemplate.gradl im Unity-Projekt notwendig:

```csharp
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar'])
    implementation 'androidx.appcompat:appcompat:1.1.0'
}
```
AppCompat ist eine Android-Unterstützungsbibliothek. Diese ermöglicht das Apps, welche mit einer neueren Version von Android entwickelt wurden, auch mit älteren Versionen kompatibel ist. 


In Share.cs wird UnityEngine.AndroidJavaClass und UnityEngine.AndroidJavaObject verwendet. Mit diesen können alle nativen Android Klassen benutzt werden. Dabei handelt es sich um Unity-Darstellungen generischer Instanzen von Java.lang.Class und Java.lang.Object, sprich die Instanzen von AndroidJavaClass und AndroidJavaObject verfügen über eine eins-zu-eins-Zuordnung zu einer Instanz von Java.lang.Class oder Java.lang.Object. 

**Script: Share.cs**
```csharp
public void shareOnSocialMedia()
{
    Panel.SetActive(false);

    String filePath = Screenshot.filePath;
    AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
    AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");
    ...
```

So wird die aktuelle Aktivität als AndroidJavaObject gespeichert. Um dem Android-Betriebssystem mitzuteilen, das ein Bild geteilt werden soll, wird „Intent“ (Absicht) verwendet. Ein Intent ist eine abstrakte Beschreibung einer auszuführenden Operation. In diesem Fall das senden des Screenshots.

```csharp
...
AndroidJavaClass intentClass = new AndroidJavaClass("android.content.Intent");
AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");
intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND"));

AndroidJavaObject fileObject = new AndroidJavaObject("java.io.File", filePath);

AndroidJavaClass fileProviderClass = new AndroidJavaClass("androidx.core.content.FileProvider");
```

Um den Screenshot zu teilen, werden mittels eines Objekt-Arrays 3 Parameter festgelegt. Diese sind die aktuelle Aktivität, die ApplicationID.provider und das Screenshotobjekt. Diese werden einem URI-Objekt übergeben, welcher die statische Methode getUriForFile() aufruft.
Über die Call-Methode des IntentObjects wird das URI-Objekt angehängt und der Typ als JPG festgelegt und die Aktivität gestartet.

```csharp
        ...
        object[] providerParams = new object[3];
        providerParams[0] = currentActivity;
        providerParams[1] = "com.ARTeam.AugmentingMasterpieces.provider";
        providerParams[2] = fileObject;

        AndroidJavaObject uriObject = fileProviderClass.CallStatic<AndroidJavaObject>("getUriForFile", providerParams);

        intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_STREAM"), uriObject);
       
        intentObject.Call<AndroidJavaObject>("setType", "image/jpg");

        intentObject.Call<AndroidJavaObject>("addFlags", intentClass.GetStatic<int>("FLAG_GRANT_READ_URI_PERMISSION"));

        AndroidJavaObject chooser = intentClass.CallStatic<AndroidJavaObject>("createChooser", intentObject, "....");
        currentActivity.Call("startActivity", chooser);
    }
}
```

<big><big>Bild ins Museum übertragen</big></big>

<img src="..\docs\Haris\SaveAndMuseum.gif" title="Museum" width="85%"/>

<img src="..\docs\Haris\SaveToData.png" title="Museum" width="85%"/>

Das Bild, welches der Benutzer über die Suche oder aus seiner Galerie-Applikation gewählt hat, wird gespeichert und der Benutzer wird gefragt, ob er direkt in die Museumsszene gelangen möchte.

Um an das Bild zu gelangen, wird der PersistentManager aufgerufen, welche das gewählte Bild auch nach einem Szenenwechsel hält. 

**Script: SaveToFile.cs**
```csharp
Texture2D picture = PersistentManager.Instance.picture.mainTexture as Texture2D;
```

Das Bild wird mit einem Zeitstempel im JPG im Applikationsordner unter "Raum" gespeichert.

```csharp
string fileName = System.DateTime.Now.ToString("dd-MM-yyyy-HH-mm-ss") + ".jpg";
string filePath = Application.persistentDataPath + "/Raum/" + fileName;
File.WriteAllBytes(filePath, picture.EncodeToJPG()); //speichern des Bildes im Applicationsordner
```

Der Pfad zur Datei wird dabei in einer Textdatei angefügt.

```csharp
        ...
        string pathtoTxt = Application.persistentDataPath + "/" + "MaterialForMuseum.txt";
        File.AppendAllText(pathtoTxt, filePath + Environment.NewLine);

        SceneManager.LoadScene("Museum");
        sceneCheck = true;
    }
}
```
--- 
# Testing

Von: <a href="https://jira.student.fiw.fhws.de:8443/secure/ViewProfile.jspa?name=k41016">Simon Weickert</a>

Das Testing für die AR-Szenen wurde manuell ausgeführt.
Um die Konsole während dem Testen auf dem Handy sehen zu können, wurde ein eigenes Logger Script geschrieben. Dieses Skript wird direkt im Hauptmenu geladen und ist in allen Szenen, fürs Debugging, sichtbar. Um eigene Lognachrichten zu schreiben, können z.B. `Debug.Log()` oder `Debug.LogError()` verwendet werden.
In der Startmethode, der Logger Klasse, wird die Methode `Log` als Listener des Events `Application.logMessageReceived` hinzugefügt.
Somit wird bei jedem neun Protokoll die folgende Methode aufgerufen und übergeben wird hierbei der `Protokolltext`, `Stacktrace` und `Protokolltyp`.
```csharp
public void Log(string logString, string stackTrace, LogType type)
{
    output = logString;
    stack = stackTrace;
    if (type != LogType.Warning)
    {
        if (stack.Length > 0)
        {
            output = output + "\n -> Stracktrace: " + stack;
        }
        myLog = output + "\n" + myLog;
    }
    if (myLog.Length > 5000)
    {

        myLog = myLog.Substring(0, 4000);
    }
}
```
Warnung werden nicht mit ausgegeben, da diese häufig nicht von großer Bedeutung waren, aber viele Zeilen in Anspruch genommen haben. Um Speicherplatz nicht unnötig zu belasten, wird Protokollstring um die letzten 1000 Charakter gekürzt, sobald darin 5000 Charakter stehen.

Die Loggingdaten werden in einer GUI box oben links im Bildschirm angezeigt.
```csharp
void OnGUI()
{
    textStyle.fontSize = 20;
    textStyle.normal.textColor = Color.cyan;
    GUI.Label(new Rect(20, 20, 300, Screen.height - 20), myLog, textStyle);
}
```

# Bekannte Bugs

In diesem Abschnitt werden kurz bekannte Probleme der App behandelt und mögliche Lösungsansätze beschrieben.

Von: <a href="https://jira.student.fiw.fhws.de:8443/secure/ViewProfile.jspa?name=k45449">Daniel Stöcklein</a>

**Die "MoveDevice" Animation verschwindet sofort nach Wechsel von der Bilder-Szene in die Skulpturen-Szene und umgekehrt.**

> Die Animation verschwindet, sobald eine AR-Plane erkannt wurde. Diese AR-Plane bleibt allerdings selbst nach einem Szenenwechsel bestehen, deswegen sieht man die Animation kurz bevor sie verschwindet. Ein möglicher Lösungsansatz wäre für die AR-Plane für die Bilder-Szene und für die Skulpturen-Szene separat erkennen zu lassen.

**Das Blättern in der Rahmenauswahl-Szene ist zu empfindlich**

> Dazu müssen einige Änderungen am Asset vorgenommen werden. Die Touch-Empfindlichkeit wird nicht abgefragt bzw. berechnet, da es möglicherweise zu Problemen mit dem Zentrieren von Bilderrahmen und den HandleFlick Einstellungen kommen wird. Mögliche Lösung wäre, Flicks zu ignorieren bzw. diese nicht in einen Swipe umzuwandeln.

Von: <a href="https://jira.student.fiw.fhws.de:8443/secure/ViewProfile.jspa?name=k42282">Haris Hodzic</a> 

**Beleuchtungsproblem im Museum**

> Die Beleuchtungsproblematik im Museumsraum wurde gelöst, tauchte aber dann wieder auf. Die Szene war stark überbeleuchtet trotz korrekter Qualitäts- und Playersettings. Das führte dazu, dass der Modus auf **Baked** geändert werden musste, da der Bug nicht behoben werden konnte.

**Texturen aus Native Gallery können im Museum nicht angezeigt werden**

> Die Texturen aus der Gallery haben einen Leseschutz, wodurch der Zugriff mit der Methode `SaveToTxtData` nicht möglich ist.
Möglicher Lösungsansatz wäre es den Texturimportsettings die Schreib- und Lesezugriffe anzupassen.

---

# Fazit

Von: <a href="https://jira.student.fiw.fhws.de:8443/secure/ViewProfile.jspa?name=k45449">Daniel Stöcklein</a>

Augmented Reality hat in den letzten Jahren sowohl Hardware- als auch die Sofwaretechnisch einen enormen Fortschritt erlebt. Die Oberflächenerkennung ohne Hilfe von definierten Markern würde sich ohne moderne Sensorik und geeigneter Frameworks als sehr komplexe Aufgabe darstellen. ARFoundation und die dazugehören Frameworks ARCore und ARKit ermöglichen über aktuelle Smartphone-Kameras eine sehr präzise Umgebungserkennung.

Das Ziel der im Rahmen dieses Projektes entwickelten App war es, den Anwender durch die Visualisierung der Bilder und der Skulpturen, die AR Möglichkeiten aufzuzeigen. Die erfolgreiche Erkennung des Raumes ist entscheidend, um einen positiven Eindruck beim Anwender zu hinterlassen.


# Ausblick

Von: <a href="https://jira.student.fiw.fhws.de:8443/secure/ViewProfile.jspa?name=k41016">Simon Weickert</a> 

Die Verwendung von Unity ist in vielerlei Hinsicht schon sehr fortgeschritten. Jedoch gab es bisher immer noch viele Schwierigkeiten beim Testen, da dafür zuerst die APK gebuildet werden muss und dann bei der Wiedergabe auf dem Handy keine Verbindung mehr zu Unity besteht. Lösungen für direkte Previews auf dem Handy gab es zwar schon, diese werden in der Unity Version 2019 aber nicht mehr unterstützt. Dieses Problem hat Unity aber in Angriff genommen und somit dieses Jahr im Juni eine neue Entwicklungsumgebung entwickelt. Diese nennt sich Mixed Reality Studio, kurz MARS und bietet die Möglichkeit, AR Funktionalitäten in einer Simulation im Editor zu testen. So kann viel Zeit und Arbeit gespart werden. Funktionen, die sonst die Interaktion zwischen der Sensorik und der realen Umgebung benötigen, können nun direkt manuell und auch automatisiert getestet werden. Leider kostet diese Umgebung pro Person und pro Monat 46€, weshalb eine Investition sich für kleinere Projekte im Studium nicht wirklich rentiert.

Zukünftig gibt es neben dieser Softwarelösungen auch kontinuierlichen Fortschritt in Software und Hardware. Die Smartphones bekommen immer mehr Funktionen und verbesserte Komponenten. Mithilfe von Tiefensensoren können Tiefenkarten berechnet werden, jedoch sind diese noch nicht für die breite Masse zugänglich. Die Berechnung dieser Karten ohne entsprechende Sensoren sind über Algorithmen möglich, jedoch benötigen diese viel Rechenleistung und sind noch nicht effektiv nutzbar.

In der Zukunft werden AR-Anwendungen für die Gesellschaft und vor allem für die Wirtschaft eine große Rolle spielen. Alltägliche Situationen können durch die AR Technologie enorm vereinfacht werden und somit wird die Wahrnehmung der Anwender erweitert.