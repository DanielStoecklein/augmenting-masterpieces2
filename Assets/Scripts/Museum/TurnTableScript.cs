﻿//Author: Haris Hodzic

using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.IO;

public class TurnTableScript : MonoBehaviour
{

    [SerializeField] GameObject smallPicture;
    [SerializeField] Transform picturePanel;

    public void Start()
    {
        List<Sprite> pictures = ProcessImages();

        for (int i = 0; i < pictures.Count; i++)
        {
            GameObject clone  = (GameObject)Instantiate(smallPicture);
            clone.GetComponentInChildren<Image>().sprite = pictures[i];
            clone.transform.SetParent(picturePanel);
            clone.name = "Clone" + i;
        }
    }

    public static List<Sprite> ProcessImages()
    {
        string txtPath = Application.persistentDataPath + "/" + "MaterialForMuseum.txt";
        List<string> picture_name_list = new List<string>();

        StreamReader sr = new StreamReader(txtPath);
        string text;
        
        while ((text = sr.ReadLine()) != null){

            picture_name_list.Add(text);
        } 

        List<Sprite> list = new List<Sprite>();
        foreach (string obj in picture_name_list)
        {
            byte[] bytes = File.ReadAllBytes(obj);
            Texture2D texture = new Texture2D(2, 2);
            texture.LoadImage(bytes);
            Sprite sprite = Sprite.Create(texture, new Rect(0.0f, 0.0f, texture.width, texture.height), new Vector2(0f, 0f), 100f);
            list.Add(sprite);
        }

        return list;
    }
}