﻿//Author: Haris Hodzic

using UnityEngine;

public class RayCastPicture : MonoBehaviour
{
    public GameObject panel;
    private UIController uiController;
    private bool uiTouched;


    private void Awake()
    {
        uiController = new UIController();
    }

    public void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            uiTouched = uiController.IsTouchOverUIObject(Input.mousePosition);

            if (!uiTouched)
            {
                if (Physics.Raycast(ray, out RaycastHit hit, 6))
                {
                    if (hit.transform.name == "Paint")
                    {

                        if (panel.gameObject.activeSelf == false)
                        {
                            panel.gameObject.SetActive(true);

                        }
                        else
                        {
                            panel.gameObject.SetActive(false);
                        }
                    }
                }
            }

        }
        staticRaycast.Update();
    }

}