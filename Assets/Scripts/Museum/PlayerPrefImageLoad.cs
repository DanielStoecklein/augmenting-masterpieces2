﻿//Author: Haris Hodzic

using System;
using UnityEngine;

public class PlayerPrefImageLoad : MonoBehaviour
{
    public GameObject paint;
    void Start()
    {
        Texture2D texture = new Texture2D(1024, 768);
        if (PlayerPrefs.HasKey(paint.transform.parent.transform.parent.name))
            
        {
            string texAsString = PlayerPrefs.GetString(paint.transform.parent.transform.parent.name);
            byte[] texAsByte = Convert.FromBase64String(texAsString);
            texture.LoadImage(texAsByte);
        }

        paint.GetComponent<Renderer>().material.SetTexture("_MainTex", texture);
    }

}
