﻿//Author: Haris Hodzic

using System.Collections;
using UnityEngine;
using UnityEngine.UI;


public class ShowWelcomeTxt : MonoBehaviour
{

    public GameObject TutorialObj;
    public Text welcomeTxt;
    public GameObject Joystickcanvas;

    void Start()
    {
        if(PlayerPrefs.HasKey("Tutorial") == false)
        {
            StartCoroutine(showWelcomeMessage());
           
        } else
        {
            TutorialObj.SetActive(false);
            Joystickcanvas.SetActive(true);
        }
    }

    private IEnumerator showWelcomeMessage()
    {
            yield return new WaitForSeconds(2f);
            welcomeTxt.text = "Mit den Joysticks links und Rechts können Sie sich im Museum bewegen";
            yield return new WaitForSeconds(2f);
            Joystickcanvas.SetActive(true);
            yield return new WaitForSeconds(2f);
            welcomeTxt.text = "Durch klicken auf eine Wand erscheint die Bilderauswahl";
            yield return new WaitForSeconds(3f);
            welcomeTxt.text = "Durch auswahl eines Bildes erscheint dieses auf Ihrer gewählten Wand";
            yield return new WaitForSeconds(3f);
            TutorialObj.SetActive(false);
            PlayerPrefs.SetString("Tutorial", "Finished");
    }

}