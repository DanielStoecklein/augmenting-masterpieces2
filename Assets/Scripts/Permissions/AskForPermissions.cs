﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Android;

public class AskForPermissions : MonoBehaviour
{

    /// <summary>
    /// Asks for needed ungranted permissions on startup
    /// </summary>
    void Start()
    {
        if (Application.platform == RuntimePlatform.Android)
            StartCoroutine(AskForNecessaryPermissionsAndroid());
        if (Application.platform == RuntimePlatform.IPhonePlayer)
            StartCoroutine(AskForNecessaryPermissionsIOS());


        string[] inFolNam = { "Screenshots", "3dObjekte", "Bilder", "Rahmen", "Podeste", "Raum", "Sonstige Dateien" };
        string[] exFolNam = { "Screenshots", "Bilder" };
        Folders.CreateInternalFolders(inFolNam);
        Folders.CreateExternalFoldersAndroid(exFolNam);
    }


    /// <summary>
    /// Asks for Permissions considered necessary for Runnning the App on IOS 
    /// </summary>
    private IEnumerator AskForNecessaryPermissionsIOS()
    {
        PermissionsIOS.AskForCameraPermission();
        yield return new WaitForEndOfFrame();
    }

    /// <summary>
    /// Asks for Permissions considered necessary for Runnning the App on Android 
    /// </summary>
    private IEnumerator AskForNecessaryPermissionsAndroid()
    {
        List<bool> permissions = new List<bool>() { false, false };
        List<bool> permissionsAsked = new List<bool>() { false, false };
        List<Action> actions = new List<Action>()
    {
        new Action(() => {
            permissions[0] = Permission.HasUserAuthorizedPermission(Permission.ExternalStorageWrite);
            if (!permissions[0] && !permissionsAsked[0])
            {
                Permission.RequestUserPermission(Permission.ExternalStorageWrite);
                permissionsAsked[0] = true;
                permissions[0] = Permission.HasUserAuthorizedPermission(Permission.ExternalStorageWrite);
                return;
            }
        }),
        new Action(() => {
            permissions[1] = Permission.HasUserAuthorizedPermission(Permission.Camera);
            if (!permissions[1] && !permissionsAsked[1])
            {
                Permission.RequestUserPermission(Permission.Camera);
                permissionsAsked[1] = true;
                permissions[1] = Permission.HasUserAuthorizedPermission(Permission.Camera);
                return;
            }
        })

    };
        for (int i = 0; i < permissionsAsked.Count;)
        {
            actions[i].Invoke();
            if (permissions[i])
            {
                ++i;
            }
            yield return new WaitForEndOfFrame();
        }
    }

}
