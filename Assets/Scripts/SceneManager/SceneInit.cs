﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneInit : MonoBehaviour
{
    public void loadSceneLoader(string sceneName)
    {
        GameObject sceneManager = GameObject.FindGameObjectWithTag("Manager");
        SceneLoader sceneLoader = sceneManager.GetComponent<SceneLoader>();
        sceneLoader.LoadScene(sceneName);
    }

}
