﻿//Autor: Haris Hodzic
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Museum : MonoBehaviour
{
   public void loadMuseumScene() => SceneManager.LoadScene("Museum");  
}
