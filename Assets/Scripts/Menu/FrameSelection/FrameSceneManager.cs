﻿using UnityEngine;
using UnityEngine.SceneManagement;

//Author: Daniel Stöcklein

/// <summary>Simple class to handle scene switches</summary>
public class FrameSceneManager : MonoBehaviour
{
    [SerializeField]
    private string previousScene;

    /*
    *...to next scene is now in script 'Menu.cs'
    private void GoToNextScene()
    {
        SceneManager.LoadSceneAsync(nextScene);
    }
    */

    public void GoToPreviousScene()
    {
        SceneManager.LoadSceneAsync(previousScene);
    }
}
