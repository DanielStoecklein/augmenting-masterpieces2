﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
//Author: Daniel Stöcklein

/// <summary>Singleton Pattern - has only a single globally accessible instance available at all times.</summary>
public class PersistentManager : MonoBehaviour
{
    public static PersistentManager Instance { get; private set; }

    public string frameName; //stores the name of the selected frame, persistent in all scene.
    public Material picture;
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
            
        }
        else
        {
            Destroy(gameObject);
        }
    }
}
