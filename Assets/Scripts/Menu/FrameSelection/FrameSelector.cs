﻿using UnityEngine;
//Author: Daniel Stöcklein

/// <summary>
/// Tells Persistent Manager, which Picture-Frame got selected
/// </summary>
public class FrameSelector : MonoBehaviour
{
	public void Select (string name)
	{
		PersistentManager.Instance.frameName = name;
	}
}
