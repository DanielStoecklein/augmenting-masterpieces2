﻿
using UnityEngine;
using UnityEngine.SceneManagement;
public class SzenenWechsel : MonoBehaviour
{
    public void bestimmteSzeneLaden(string s) {
        SceneManager.LoadSceneAsync(s);
    }
}
