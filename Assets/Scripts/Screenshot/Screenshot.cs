﻿//Author: Haris Hodzic

using System;
using System.Collections;
using System.IO;
using UnityEngine;


public class Screenshot : MonoBehaviour
{
    //VariablenDeklaration
    public GameObject Panel;
    public static String filePath;
    private bool isFocus = false;
    private bool isProcessing = false;

    void OnApplicationFocus(bool focus)
    {
        isFocus = focus;
    }

    public void OnShareButtonClick()
    {
        //Daniel Stöcklein - Schließe Side-Menu
        GameObject sideMenu = GameObject.FindGameObjectWithTag("SideMenu");
        sideMenu.GetComponent<SimpleSideMenu>().Close();
        //

        if (!isProcessing) // erst nachdem TakeScreenshotAndSave() - Methode abgeschlossen ist, kann sie erneut durchgeführt werden
        {
            StartCoroutine(TakeScreenshotAndSave()); //Coroutine kann die Ausführung pausieren und später wieder aufnehmen
        }
    }

    private IEnumerator TakeScreenshotAndSave()
    {
        Panel.SetActive(true);

        isProcessing = true;

        //Dateiname, bestehend aus zeitstempel und Dateiendung
        string fileName = System.DateTime.Now.ToString("dd-MM-yyyy-HH-mm-ss") + ".jpg";

        //UI verbergen
        yield return null; //Ausführung wird paussiert und im nächsten frame wieder aufgenommen
        GameObject.Find("Canvas").GetComponent<Canvas>().enabled = false;

        //Screen wird gerendert
        yield return new WaitForEndOfFrame(); //

        //2D Textur vom Bildschirm wird erstellt
        Texture2D screenImage = new Texture2D(Screen.width, Screen.height);
        screenImage.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
        screenImage.Apply();

        //Konvertierung in JPG und speichern in Byte Array
        byte[] imageBytes = screenImage.EncodeToJPG();

        //Pfad und Speichern
        filePath = Application.persistentDataPath + "/Screenshots/" + fileName;

        File.WriteAllBytes(filePath, screenImage.EncodeToJPG()); //speichern des Bildes im Applicationsordner

        NativeGallery.SaveImageToGallery(imageBytes, "Camera", fileName, null);
       
        
        if (File.Exists(filePath))
        {
            Toast_Message.messageTxt("Bild gespeichert!");
        }
        else
        {
            Toast_Message.messageTxt("Fehler aufgetreten!");
        }

        //UI wieder anzeigen
        GameObject.Find("Canvas").GetComponent<Canvas>().enabled = true;

        yield return new WaitUntil(() => isFocus);
        isProcessing = false;

        
          
    }
}

