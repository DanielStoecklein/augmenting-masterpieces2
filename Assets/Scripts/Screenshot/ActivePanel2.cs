﻿//Author: Haris Hodzic

using UnityEngine;

public class ActivePanel2 : MonoBehaviour
{
    public GameObject panel2;
    public void BtnOnClick()
    {
        //Daniel Stöcklein - Schließe Side-Menu
        GameObject sideMenu = GameObject.FindGameObjectWithTag("SideMenu");
        sideMenu.GetComponent<SimpleSideMenu>().Close();
        //

        panel2.SetActive(true);
    }

   
}
