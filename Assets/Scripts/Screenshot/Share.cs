﻿//Author: Haris Hodzic

using System;
using UnityEngine;


public class Share : MonoBehaviour
{
    public GameObject Panel;

    public void shareOnSocialMedia()
    {
        Panel.SetActive(false);

        String filePath = Screenshot.filePath;
        AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");

        AndroidJavaClass intentClass = new AndroidJavaClass("android.content.Intent");
        AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");
        intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND"));

        //Dateiobjekt vom Screenshot erstellen
        AndroidJavaObject fileObject = new AndroidJavaObject("java.io.File", filePath);

        //FileProviderklasse wird erstellt (androidx.core.content.FileProvider verwenden) 
        AndroidJavaClass fileProviderClass = new AndroidJavaClass("androidx.core.content.FileProvider");

        //Array vom Typ Object wird erstellt und es wird 
        //currentActivity, PackageName(Project Settings->Player->Other Settings->Identification), Bildpfad ihm zugewiesen
        object[] providerParams = new object[3];
        providerParams[0] = currentActivity;
        providerParams[1] = "com.ARTeam.AugmentingMasterpieces.provider";
        providerParams[2] = fileObject;

        //Objekt wird erstellt mittels getUriForFile()-Methode, dem Parameter übergeben werden
        AndroidJavaObject uriObject = fileProviderClass.CallStatic<AndroidJavaObject>("getUriForFile", providerParams);

        intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_STREAM"), uriObject);
       
        //Typ wird als JPG-Bild festgelegt
        intentObject.Call<AndroidJavaObject>("setType", "image/jpg");

        //Berechtigung wird gegeben um URI zu lesen
        intentObject.Call<AndroidJavaObject>("addFlags", intentClass.GetStatic<int>("FLAG_GRANT_READ_URI_PERMISSION"));

        AndroidJavaObject chooser = intentClass.CallStatic<AndroidJavaObject>("createChooser", intentObject, "....");
        currentActivity.Call("startActivity", chooser);
    }
}

