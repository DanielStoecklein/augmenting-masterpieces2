﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Android;

public class Folders : MonoBehaviour
{

    ///<summary>Creates folders that are located in the directory directectly associated with the app if not already in place.
    ///<br/>Android: storage/emulated/0/Android/data/[name].[of].[app-package]/files/[folder-name]</summary>
    ///<param name="folderNames"> Array of folder names to be created</param>
    public static void CreateInternalFolders(string[] folderNames)
    {
        foreach (string fn in folderNames)
        {
            string path = Path.Combine(Application.persistentDataPath, fn);
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
        }
    }



    /// <summary>
    /// Method to get  internal folders Path
    /// </summary>
    /// <param name = "Foldername"> 
    /// Name of the folder you search for <br/>
    /// Insert null if internal Application base is needed. </param>
    /// <returns> Path to folder if it exists <br/>
    /// null if else</returns>
    public static string GetInternalFolder(string Foldername)
    {
        if (Foldername != null||Foldername!="")
        {
            if (Directory.Exists(Path.Combine(Application.persistentDataPath, Foldername)))
                return Path.Combine(Application.persistentDataPath, Foldername);
            return null;
        }
        return Application.persistentDataPath;
    }


    ///<summary>Creates folders that are located in directories open to all apps with the necessary rigths to wright and read data if not already in place.
    ///<br/>Android: storage/emulated/0/[app-name]/[folder-name]
    ///<param name="folderNames"> Array of folder names to be created</param>
    public static void CreateExternalFoldersAndroid(string[] folderNames)
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            foreach (string fn in folderNames)
            {
                string path = Path.Combine(GetAndroidExternalStoragePath(), Application.productName, fn);

                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
            }
        }
    }

    /// <summary>
    /// Method to get  external folders Path for Android
    /// </summary>
    /// <param name = "Foldername"> 
    /// Name of the folder you search for <br/>
    /// Insert null if external Application base is needed. </param>
    /// <returns> Path to folder if it exists <br/>
    /// null if else</returns>
    public static string GetExternalFolderAndroid(string Foldername)
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            if (Foldername != null || Foldername != "")
            {
                if (Directory.Exists(Path.Combine(GetAndroidExternalStoragePath(), Application.productName, Foldername)))
                    return Path.Combine(GetAndroidExternalStoragePath(), Application.productName, Foldername);
                return null;
            }
            return Path.Combine(GetAndroidExternalStoragePath(), Application.productName);
        }
        return null;
    }


    /**
     * <summary> Finds the path where Android stores its external data</summary>
     * <returns>returns a path as a string </returns>
     */
    public static string GetAndroidExternalStoragePath()
    {
        string path = "";
        try
        {
            AndroidJavaClass jc = new AndroidJavaClass("android.os.Environment");
            path = jc.CallStatic<AndroidJavaObject>("getExternalStorageDirectory").Call<string>("getAbsolutePath");

        }
        catch (Exception e)
        {
            Debug.Log(e.Message);
        }
        return path;
    }

}

