﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingleUnsplashResult : MonoBehaviour
{

    public UnsplashSearch.Photo photo;
    public Texture thumb;
    public Texture FullImmage;

    internal string GetAttributons()
    {
        return "Photo by "+photo.user.name +"\n"+photo.links.html;

    }


    /*// Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }*/
}
