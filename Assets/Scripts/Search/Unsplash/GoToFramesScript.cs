﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
public class GoToFramesScript : MonoBehaviour
{
    public GameObject previewImage;
    public void GoToFramesOnClick() 
    {
        Material m = Resources.Load("m", typeof(Material)) as Material;
        m.SetTexture("_MainTex", previewImage.GetComponent<RawImage>().mainTexture);
    }
}
