﻿using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace UnsplashSearch
{
    [Serializable]
    public class SearchResults
    {
        public int total;
        public int total_pages;
        public Photo[] results;
        public LinkHeader header;



    }
    [Serializable]
    public class Photo
    {
        public string id;
        public string created_at;
        public int width;
        public int height;
        public string color;
        public string description;
        public string alt_description;
        public UnsplashUser user;
        public PhotoUrls urls;
        public PhotoLinks links;
    }

    [Serializable]
    public class UnsplashUser
    {
        public string id;
        public string username;
        public string name;
        public string instagramm_username;
        public string twitter_username;
        public string portfolio_url;
        public ProfileImage profile_image;
        public UserLinks links;
    }
    [Serializable]
    public class ProfileImage
    {
        public string small;
        public string medium;
        public string large;
    }

    [Serializable]
    public class UserLinks
    {
        public string self;
        public string html;
        public string photos;
        public string likes;
    }

    [Serializable]
    public class PhotoUrls
    {
        public string raw;
        public string full;
        public string regular;
        public string small;
        public string thumb;
    }

    [Serializable]
    public class PhotoLinks
    {
        public string self;
        public string html;
        public string download;
    }

    [Serializable]
    public class LinkHeader
    {
        public string FirstLink;
        public string PrevLink;
        public string NextLink;
        public string LastLink;

        public static LinkHeader LinksFromHeader(string linkHeaderStr)
        {
            LinkHeader linkHeader = null;

            if (!string.IsNullOrWhiteSpace(linkHeaderStr))
            {
                string[] linkStrings = linkHeaderStr.Split(',');

                if (linkStrings != null && linkStrings.Any())
                {
                    linkHeader = new LinkHeader();

                    foreach (string linkString in linkStrings)
                    {
                        var relMatch = Regex.Match(linkString, "(?<=rel=\").+?(?=\")", RegexOptions.IgnoreCase);
                        var linkMatch = Regex.Match(linkString, "(?<=<).+?(?=>)", RegexOptions.IgnoreCase);

                        if (relMatch.Success && linkMatch.Success)
                        {
                            string rel = relMatch.Value.ToUpper();
                            string link = linkMatch.Value;

                            switch (rel)
                            {
                                case "FIRST":
                                    linkHeader.FirstLink = link;
                                    break;
                                case "PREV":
                                    linkHeader.PrevLink = link;
                                    break;
                                case "NEXT":
                                    linkHeader.NextLink = link;
                                    break;
                                case "LAST":
                                    linkHeader.LastLink = link;
                                    break;
                            }
                        }
                    }
                }
            }

            return linkHeader;
        }
    }
}



