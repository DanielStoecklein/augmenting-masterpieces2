﻿using Newtonsoft.Json;
using System.Collections;
using System.Security.Policy;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

namespace UnsplashSearch
{
    public class UnsplashRequestHandler : MonoBehaviour
    {


        private const string secretkey = "kTC4adtBtenbjWlRfR-_s6OTR7xPBDKg7ZpMuO2FWaE";
        private const string acceskey = "lw_UQYcYNX9uJQB3qpzrdF7-QxqW8NaqZzFhkBNpN9o";

        public SearchResults lastSearchResult;
        public GameObject Suchfeld;
        public UnsplashImageLoader imageloader;
        public bool loading;

        public void InitSearchPhotos()
        {

            if (Suchfeld.GetComponent<Text>().text == null || Suchfeld.GetComponent<Text>().text == "")
            {
                Debug.Log("Leeres Suchfeld.");
                return;
            }

            UnsplashSearchUrlBuilder unsplashSearchUrlBuilder = new UnsplashSearchUrlBuilder();
            unsplashSearchUrlBuilder.Query = Suchfeld.GetComponent<Text>().text;
            unsplashSearchUrlBuilder.Per_Page = 24;
            string searchurl = unsplashSearchUrlBuilder.Build();

            Debug.Log("Suchwort ist " + Suchfeld.GetComponent<Text>().text);
            StartCoroutine(SearchPhotos(searchurl));

        }

        public IEnumerator SearchPhotos(string searchurl)
        {

            UnityWebRequest searchrequest = UnityWebRequest.Get(searchurl);
            searchrequest.SetRequestHeader("Authorization", "Client-ID " + acceskey);

            yield return searchrequest.SendWebRequest();

            if (searchrequest.isNetworkError || searchrequest.isHttpError)
            {
                Debug.Log(searchrequest.error);
            }
            else
            {
                string answerbody = searchrequest.downloadHandler.text;
                lastSearchResult = DeserializeResponse(answerbody);
                lastSearchResult.header = LinkHeader.LinksFromHeader(searchrequest.GetResponseHeader("Link"));
                imageloader.UpdateThumbsUnsplash();
                loading = false;
            }

        }

        public void LoadNextPage()
        {
            loading = true;
            Debug.Log("Nächste Ergebnisse werden geladen");
            StartCoroutine(SearchPhotos(lastSearchResult.header.NextLink));
        }

        public SearchResults DeserializeResponse(string json)
        {
            SearchResults sr = JsonConvert.DeserializeObject<SearchResults>(json);
            return sr;
        }


        internal class UnsplashSearchUrlBuilder
        {
            private string query = null;

            public string BaseURL { get; set; } = "https://api.unsplash.com/search/photos";
            public string Query { get => query; set => query = UnityWebRequest.EscapeURL(value); }
            public int Page { get; set; } = 0;
            public int Per_Page { get; set; } = 0;
            public string Order_By { get; set; } = null;
            public string Color { get; set; } = null;
            public string Orientation { get; set; } = null;


            public string Build()
            {
                string url = null;
                if (Query != null)
                {
                    url = BaseURL + "?query=" + Query;

                    if (Page != 0)
                        url = url + "&page=" + Page;
                    if (Per_Page != 0)
                        url = url + "&per_page=" + Per_Page;
                    if (Order_By != null)
                        url = url + "&order_by=" + Order_By;
                    if (Color != null)
                        url = url + "&color=" + Color;
                    if (Orientation != null)
                        url = url + "&orientation=" + Orientation;
                }
                return url;
            }

        }
    }


}
