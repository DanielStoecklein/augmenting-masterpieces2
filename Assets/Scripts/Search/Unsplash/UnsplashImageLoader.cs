﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnsplashSearch;

public class UnsplashImageLoader : MonoBehaviour
{
    public UnsplashRequestHandler UnsplashRequestHandler;
    public GameObject ResultImmages;
    public GameObject Preview;
    public GameObject SearchBar;
    public GameObject ThumbPrefab;


    internal void UpdateThumbsUnsplash()
    {
        Debug.Log("Thumbnails Loading");
        SearchResults sr = UnsplashRequestHandler.GetComponent<UnsplashRequestHandler>().lastSearchResult;

        foreach (Photo ph in sr.results)
        {
            GameObject obj = Instantiate(ThumbPrefab);
            obj.GetComponent<SingleUnsplashResult>().photo = ph;
            StartCoroutine(DownloadThumbnails(ph.urls.thumb, obj));
        }
    }


    internal IEnumerator DownloadThumbnails(string MediaUrl, GameObject obj)
    {
        UnityWebRequest request = UnityWebRequestTexture.GetTexture(MediaUrl);
        yield return request.SendWebRequest();
        if (request.isNetworkError || request.isHttpError)
            Debug.Log(request.error);
        else
        {
            obj.GetComponentInChildren<RawImage>().texture = ((DownloadHandlerTexture)request.downloadHandler).texture;
            Texture texture = obj.GetComponentInChildren<RawImage>().texture;
            obj.GetComponentInChildren<RawImage>().GetComponent<AspectRatioFitter>().aspectRatio = ((float)texture.width / (float)texture.height);
            obj.transform.parent = (ResultImmages.transform);

            obj.GetComponent<SingleUnsplashResult>().thumb = texture;
            UnityEngine.Events.UnityAction call = delegate { OnUnsplashButtonClick(obj.GetComponent<SingleUnsplashResult>()); };
            obj.GetComponent<Button>().onClick.AddListener(call);
        }
    }

    private void OnUnsplashButtonClick(SingleUnsplashResult singleUnsplashResult)
    {

        Debug.Log("Thumb was clicked");
        Preview.SetActive(true);
        Preview.GetComponentInChildren<PreviewImageScript>().result = singleUnsplashResult;
        StartCoroutine(Preview.GetComponentInChildren<PreviewImageScript>().LoadPicture());
        if (SearchBar.activeSelf)
        {
            SearchBar.SetActive(false);
        }
    }

    public void DestroyThumbs()
    {
        int NumberOfResults = ResultImmages.transform.childCount;
        for (int i = NumberOfResults - 1; i >= 0; i--)
        {
            Destroy(ResultImmages.transform.GetChild(i).gameObject);
        }
    }
}

