﻿
using UnityEngine;
using UnityEngine.UI;
using UnsplashSearch;

public class InputFieldScript : MonoBehaviour
{
    public InputField inputField;
    public GameObject RequestHandler;

    public void UnsplashEndEdit()
    {
        if (TouchScreenKeyboard.isSupported && inputField.touchScreenKeyboard.status == TouchScreenKeyboard.Status.Done || Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter) || Input.GetButtonDown("Submit"))
        {
            RequestHandler.GetComponent<UnsplashRequestHandler>().InitSearchPhotos();
            RequestHandler.GetComponent<UnsplashImageLoader>().DestroyThumbs();
        }
    }
    public void PolyEndEdit()
    {
        if (TouchScreenKeyboard.isSupported && inputField.touchScreenKeyboard.status == TouchScreenKeyboard.Status.Done || Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter) || Input.GetButtonDown("Submit"))
        {
            RequestHandler.GetComponent<PolyRequestHandler>().InitSearchObjects();
            RequestHandler.GetComponent<PolyImageLoader>().DestroyThumbs();
        }
    }
}
