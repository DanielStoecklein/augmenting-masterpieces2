﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GalleryScript : MonoBehaviour
{

    public GameObject preview;
    public GameObject GalleryButton;
    public GameObject GoToFramesButton;




    public void PickImage()
    {
        NativeGallery.GetImageFromGallery(HandleMediaPickCallback, "Pick Image for the AR Frame");
    }

    private void HandleMediaPickCallback(string path)
    {
#if UNITY_EDITOR
        path = "C:/Users/Thomas/Pictures/Duerer_-_Studie_Reiter_1495.jpg";
#endif

        Texture2D image = NativeGallery.LoadImageAtPath(path);

        GoToFramesButton.SetActive(true);
        preview.SetActive(true);
        preview.GetComponent<RawImage>().texture = image;

        AdjustImageSize(image, preview.GetComponent<RawImage>());


    }

    private const float StandardImageSize = 420f;
    private const float MaxWidth = 1760f;
    private const float MaxHeight = 460f;

    private void AdjustImageSize(Texture2D image, RawImage preview)
    {
        float width = image.width;
        //Debug.Log(image.width + " = orig width");

        float height = image.height;
        //Debug.Log(image.height + " = orig height");

        float heightWidthRatio = (height / width);
        //Debug.Log(heightWidthRatio + " = h/w ratio");
        if (heightWidthRatio > 1f)
        {
            //Debug.Log(heightWidthRatio + " 1");
            preview.rectTransform.sizeDelta = new Vector2(StandardImageSize, StandardImageSize * heightWidthRatio);
        }
        else
        {
            //Debug.Log(heightWidthRatio + " 2");
            preview.rectTransform.sizeDelta = new Vector2(StandardImageSize / heightWidthRatio, StandardImageSize);
        }

        //Skalliere die Bilder so, dass sie die Maximalausmaße nicht überschreiten
        //->zu hoher wert wird auf max wert gestellt 
        //->anderer Wert angepasst um relationen nicht zu verändern
        if (preview.rectTransform.sizeDelta.x > MaxWidth)
        {

            float overwidth = preview.rectTransform.sizeDelta.x / MaxWidth;

            float adjustedHeight = preview.rectTransform.sizeDelta.y / overwidth;
            preview.rectTransform.sizeDelta = new Vector2(MaxWidth, adjustedHeight);
        }

        if (preview.rectTransform.sizeDelta.y > MaxHeight)
        {
            float overheight = preview.rectTransform.sizeDelta.y / MaxHeight;
            float adjustedWidth = preview.rectTransform.sizeDelta.x / overheight;
            preview.rectTransform.sizeDelta = new Vector2(adjustedWidth, MaxHeight);
        }

    }
}
