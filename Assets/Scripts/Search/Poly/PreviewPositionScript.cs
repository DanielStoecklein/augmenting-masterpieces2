﻿using PolyToolkit;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PreviewPositionScript : MonoBehaviour
{
    public SinglePolyResult result;
    public GameObject PreviewPosition;
    public GameObject Failure;
    public GameObject PreviewView;


    /*// Update is called once per frame
    void Update()
    {

    }*/

    internal void LoadObject()
    {
        PolyImportOptions options = PolyImportOptions.Default();
        options.rescalingMode = PolyImportOptions.RescalingMode.FIT;
        options.desiredSize = 1f;
        options.recenter = true;
        PolyApi.Import(result.asset, options, ImportAssetCallback);

    }

    private void ImportAssetCallback(PolyAsset asset, PolyStatusOr<PolyImportResult> result)
    {
        if (!result.Ok)
        {
            Debug.LogError("Failed to import asset. Status: " + result.Status);
            ShowFailureMessage();
            return;
        }

        Debug.Log("Successfully imported asset! ");
        foreach (Transform child in PreviewPosition.transform)
            Destroy(child.gameObject);

        transform.GetChild(0).gameObject.GetComponent<Text>().text = asset.AttributionInfo;


        /*GameObject PreviewObject = Instantiate(result.Value.gameObject, PreviewPosition.transform);
        PreviewObject.AddComponent<Rotate>();
        PreviewObject.gameObject.AddComponent<RectTransform>();
        PreviewObject.GetComponent<RectTransform>().anchoredPosition3D = new Vector3(0f, 0f, 1f);
        */

        result.Value.gameObject.transform.SetParent(PreviewPosition.transform);
        result.Value.gameObject.AddComponent<RectTransform>();
        result.Value.gameObject.GetComponent<RectTransform>().anchoredPosition3D = new Vector3(0f, 0f, 0f);


    }

    private void ShowFailureMessage()
    {
        PreviewView.SetActive(false);
        Failure.SetActive(true);
    }
}
