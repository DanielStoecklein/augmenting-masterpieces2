﻿using PolyToolkit;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class PolyImageLoader : MonoBehaviour
{
    public PolyRequestHandler PolyRequestHandler;
    public GameObject ResultImmages;
    public GameObject Preview;
    public GameObject SearchBar;
    public GameObject ThumbPrefab;
    private bool thumbnailFailureFlag;


    internal void UpdateThumbsPoly()
    {
        Debug.Log("Thumbnails Loading");
        PolyListAssetsResult result = PolyRequestHandler.GetComponent<PolyRequestHandler>().lastresult;

        foreach (PolyAsset asset in result.assets)
        {
            thumbnailFailureFlag = false;
            GameObject obj = Instantiate(ThumbPrefab);
            obj.GetComponent<SinglePolyResult>().asset = asset;
            obj.GetComponent<SinglePolyResult>().attribution = asset.AttributionInfo;
            PolyApi.FetchThumbnail(asset, FetchThumbCallback);
            if (thumbnailFailureFlag) return;
            StartCoroutine(DownloadThumbnails(asset.thumbnail.url, obj));
        }
    }

    private void FetchThumbCallback(PolyAsset asset, PolyStatus status)
    {
        if (!status.ok)
        {
            Debug.Log("Ein Thumbnail konnte nicht heruntergeladen werden");
            thumbnailFailureFlag = true;
            return;
        }
    }

    internal IEnumerator DownloadThumbnails(string MediaUrl, GameObject obj)
    {
        UnityWebRequest request = UnityWebRequestTexture.GetTexture(MediaUrl);
        yield return request.SendWebRequest();
        if (request.isNetworkError || request.isHttpError)
            Debug.Log(request.error);
        else
        {
            obj.GetComponentInChildren<RawImage>().texture = ((DownloadHandlerTexture)request.downloadHandler).texture;
            Texture texture = obj.GetComponentInChildren<RawImage>().texture;
            obj.GetComponentInChildren<RawImage>().GetComponent<AspectRatioFitter>().aspectRatio = ((float)texture.width / (float)texture.height);
            obj.transform.parent = (ResultImmages.transform);

            obj.GetComponent<SinglePolyResult>().thumb = texture;
            UnityEngine.Events.UnityAction call = delegate { OnPolyButtonClick(obj.GetComponent<SinglePolyResult>()); };
            obj.GetComponent<Button>().onClick.AddListener(call);
        }
    }

    private void OnPolyButtonClick(SinglePolyResult singlePolyResult)
    {
        Debug.Log("Thumb was clicked");
        Preview.SetActive(true);
        Preview.GetComponentInChildren<PreviewPositionScript>().result = singlePolyResult;
        Preview.GetComponentInChildren<PreviewPositionScript>().LoadObject();
        if (SearchBar.activeSelf)
        {
            SearchBar.SetActive(false);
        }
    }

    public void DestroyThumbs()
    {
        int NumberOfResults = ResultImmages.transform.childCount;
        for (int i = NumberOfResults - 1; i >= 0; i--)
        {
            Destroy(ResultImmages.transform.GetChild(i).gameObject);
        }
    }
}


