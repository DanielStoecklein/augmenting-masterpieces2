﻿using PolyToolkit;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class PolyRequestHandler : MonoBehaviour
{
    public GameObject Suchfeld;
    public PolyImageLoader ImageLoader;
    public PolyListAssetsResult lastresult;
    public bool loading;

    private void Start()
    {
        PolyApi.Init();
    }

    public void InitSearchObjects()
    {
        if (Suchfeld.GetComponent<Text>().text == null || Suchfeld.GetComponent<Text>().text == "")
        {
            Debug.Log("Leeres Suchfeld.");
            return;
        }

        PolyListAssetsRequest request = new PolyListAssetsRequest();
        request.keywords = Suchfeld.GetComponent<Text>().text;
        request.pageSize = 24;
        Debug.Log("Suchwort ist " + Suchfeld.GetComponent<Text>().text);
        PolyApi.ListAssets(request, SearchRequestCallback);
    }

    private void SearchRequestCallback(PolyStatusOr<PolyListAssetsResult> result)
    {
        if (!result.Ok)
        {
            Debug.Log("Fehler bei Anfrage. Status:" + result.Status.errorMessage);
        }

        lastresult = result.Value;
        Debug.Log(result.Value.assets.Count);
        ImageLoader.UpdateThumbsPoly();
        loading = false;

    }


    public void LoadNextPage()
    {
        loading = true;
        PolyListAssetsRequest request = new PolyListAssetsRequest();
        request.pageToken = lastresult.nextPageToken;;
        request.pageSize = 24;
        Debug.Log("Nächste Ergebnisse werden geladen");
        PolyApi.ListAssets(request, SearchRequestCallback);

    }
}
