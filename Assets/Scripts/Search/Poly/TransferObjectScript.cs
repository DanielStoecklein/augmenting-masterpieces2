﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransferObjectScript : MonoBehaviour
{
    public GameObject ObjectCarrier;
    public GameObject Preview;

    public void DontDestroy()
    {
        GameObject PolyObject = Instantiate(Preview.transform.GetChild(0).gameObject, ObjectCarrier.transform);
        PolyObject.SetActive(false);
        DontDestroyOnLoad(ObjectCarrier);
    }



}
