﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TabSystem : MonoBehaviour
{
    public void BackToMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }
    public void OpenSculptureTab()
    {
        SceneManager.LoadScene("TabSculptures");
    }
    public void OpenGalleryTab()
    {
        SceneManager.LoadScene("TabGallery");
    }
    public void OpenSearchTab()
    {
        SceneManager.LoadScene("TabSearch");
    }
}
