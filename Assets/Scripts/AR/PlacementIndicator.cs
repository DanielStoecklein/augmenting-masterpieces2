﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

//Author: Simon Weickert

public class PlacementIndicator : MonoBehaviour
{
    private GameObject markerHolder;
    private GameObject indicator;
    [SerializeField]
    Material m_indicatorPlane;
    [SerializeField]
    Material m_indicatorPlaneFinal;
    private GameObject horizontalIndicatorPlane;
    private GameObject verticalIndicatorPlane;
    private GameObject verticalPlane;
    private GameObject previewTransform;
    private GameObject lookUpArrow;

    private ObjectSpawner spawnerScript;
    private MarkerManager markerManager;
    private ARRaycastManager arRaycastManager;
    private ARPlaneManager arPlaneManager;
    private EventPublisher eventPublisher;

    private Vector2 screenMiddle = new Vector2(Screen.width / 2, Screen.height / 2);
    private List<ARRaycastHit> hits = new List<ARRaycastHit>();

    private bool firstTime = true;
    private bool markerLocked = false;
    private bool alreadyHitHorizontalPlane = false;

    private SculpturePlacer sculpturePlacer;


    ARRaycastHit arHit;


    // public void Action_OnTouchBegan(object sender, EventArgs e) //Eventaction shifted to OnTouchAndRaycastOnHorizontalARPlane
    // {

    // }

    private void Action_OnRaycastHitHorizontalARPlane(List<ARRaycastHit> hits)
    {
        if (firstTime)
        {
            spawnerScript.placePicture(previewTransform.transform.position, previewTransform.transform.rotation);
            spawnerScript.changeTransparency(0.35f);
            spawnerScript.getPlacedObject().transform.SetParent(verticalPlane.transform, true);
            ShowGameObjectIfNotYetActive(verticalPlane);
            Handheld.Vibrate();
            //TODO: Change horizontal Indicatorplane here
            horizontalIndicatorPlane.GetComponent<MeshRenderer>().material = m_indicatorPlaneFinal;
            firstTime = false;
        }
        if (markerLocked == false)
        {
            arHit = hits[hits.Count - 1];
            markerHolder.transform.position = arHit.pose.position;
            markerHolder.transform.rotation = arHit.pose.rotation;
            if (!alreadyHitHorizontalPlane) alreadyHitHorizontalPlane = true;

            ShowGameObjectIfNotYetActive(indicator);
        }
        else //Vertical Plane is activated and Marker locked => Picture Object can now be placed
        {
            // spawnerScript.objectSpawnerState(true); deactivated because picture now gets instantly placed
            markerManager.AllowNewMarker(); Debug.Log("New Marker can be placed now");
            spawnerScript.changeTransparency(1.0f);
            eventPublisher.OnRaycastHitHorizontalARPlane -= Action_OnRaycastHitHorizontalARPlane;
            eventPublisher.OnTouchAndRaycastHitHorizontalARPlane -= Action_OnTouchAndRaycastHitHorizontalARPlane;
        }
    }

    private void Action_OnTouchAndRaycastHitHorizontalARPlane(List<ARRaycastHit> hits)
    {
        if (alreadyHitHorizontalPlane == true)
        {
            //TODO: Change vertical Indicatorplane here
            ShowGameObjectIfNotYetActive(verticalPlane);
            verticalIndicatorPlane.GetComponent<MeshRenderer>().material = m_indicatorPlaneFinal;
            markerLocked = true;
        }
        Debug.Log("Event: OnTouch and raycast hit horizontal AR plane (placement indicator script)");
    }


    void ShowGameObjectIfNotYetActive(GameObject obj)
    {
        if (!obj.activeInHierarchy)
        {
            obj.SetActive(true);
            Debug.Log(obj.name + " activated!");
        }
    }


    void Start()
    {
        //get the components
        eventPublisher = FindObjectOfType<EventPublisher>();
        spawnerScript = FindObjectOfType<ObjectSpawner>();
        markerManager = FindObjectOfType<MarkerManager>();

        //Make the Function from the PlacementIndicator Script listen to the OnTouchBegan Event
        sculpturePlacer = GetComponent<SculpturePlacer>();

        if (true) // sculpturePlacer.isPolyMode() == false
        {
            eventPublisher.OnRaycastHitHorizontalARPlane += Action_OnRaycastHitHorizontalARPlane;
            eventPublisher.OnTouchAndRaycastHitHorizontalARPlane += Action_OnTouchAndRaycastHitHorizontalARPlane;
        }

        arRaycastManager = FindObjectOfType<ARRaycastManager>();
        arPlaneManager = FindObjectOfType<ARPlaneManager>();
        markerHolder = transform.gameObject;

        indicator = transform.GetChild(0).gameObject;
        verticalIndicatorPlane = indicator.transform.GetChild(1).gameObject;
        horizontalIndicatorPlane = indicator.transform.GetChild(2).gameObject;
        lookUpArrow = indicator.transform.GetChild(3).gameObject;

        verticalPlane = transform.GetChild(1).gameObject;
        previewTransform = transform.GetChild(2).gameObject;


        //hide the placement indicator at Start
        indicator.SetActive(false);
        verticalPlane.SetActive(false);
        lookUpArrow.SetActive(false);

        spawnerScript.objectSpawnerState(false);
    }
    [SerializeField]
    private float arrowVisibilityTimer = 5;

    void Update()
    {

        if (markerLocked == true)
        {
            ShowGameObjectIfNotYetActive(lookUpArrow);
            arrowVisibilityTimer -= Time.deltaTime;
            if (arrowVisibilityTimer <= 0.0f)
            {
                lookUpArrow.SetActive(false);
                this.enabled = false; //disables Placemenindicator Component on the Marker Holder 
            }

        }
    }
}
