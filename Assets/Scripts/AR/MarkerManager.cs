﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Author: Simon Weickert

public class MarkerManager : MonoBehaviour
{
    [SerializeField]
    private GameObject markerHolder;
    private GameObject currentHolder;
    // private List<GameObject> placedObjects = new List<GameObject>();
    // private List<GameObject> holderObjects = new List<GameObject>();
    private int counter = 0;

    private bool newMarkerAllowed = true;

    public void AllowNewMarker()
    {
        newMarkerAllowed = true;
    }


    void Start()
    {
        if (newMarkerAllowed)
            makeNewMarker();
        newMarkerAllowed = false;
    }



    public void makeNewMarker()
    {
        if (newMarkerAllowed)
        {
            currentHolder = Instantiate(markerHolder);
            currentHolder.name = "Marker Holder " + counter;
            currentHolder.transform.GetChild(1).name = "VerticalPlane " + counter;

            counter++;
            newMarkerAllowed = false;
        }
        /*else 
        {
            //TODO: Give user feedback that only one marker can be placed at once
        }
        */
    }

}
