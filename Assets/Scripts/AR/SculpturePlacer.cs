﻿using System.Collections;
using System.Collections.Generic;
using Lean.Touch;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using UnityEngine.SceneManagement;

//Author: Simon Weickert
public class SculpturePlacer : MonoBehaviour
{
    public class Poly
    {
        public GameObject sculpture = null;
        public GameObject marker;
        public List<Material> materialsSave = new List<Material>();
        public bool positionLocked = false;

    }

    [SerializeField]
    private Camera arCamera;
    private EventPublisher eventPublisher;
    private GameObject objectCarrier;
    [SerializeField]
    private GameObject hitMarker;
    [SerializeField]
    private Material transparentMaterial;


    private ARRaycastHit arHit;
    private bool alreadyHitHorizontalPlane = false;
    int newest = 0;
    private UIController uiController;
    private bool uiTouched;
    private List<ARRaycastHit> hits = new List<ARRaycastHit>();
    private Vector2 screenMiddle = new Vector2(Screen.width / 2, Screen.height / 2);
    private ARRaycastManager arRaycastManager;
    List<Poly> polys = new List<Poly>();
    private bool firstTime = true;
    float timer = 2f;
    private bool polyInitiated = false;
    [SerializeField]
    private GameObject pictureModeButton;

    private void OnRaycastHitHorizontalARPlane(List<ARRaycastHit> hits)
    {
        if (polys[newest].positionLocked == false)
        {
            arHit = hits[hits.Count - 1];
            polys[newest].marker.transform.position = arHit.pose.position;
            polys[newest].marker.transform.rotation = arHit.pose.rotation;
            if (!alreadyHitHorizontalPlane)
            {
                alreadyHitHorizontalPlane = true;
                polys[newest].sculpture.SetActive(true);
                polys[newest].marker.SetActive(true);
                Debug.Log("sculpture & marker set Active");
            }
        }
    }


    private void OnTouchAndRaycastHitHorizontalARPlane(List<ARRaycastHit> hits)
    {
        if (alreadyHitHorizontalPlane == true && polys[newest].positionLocked == false)
        {
            changeToTransparent(false);
            polys[newest].positionLocked = true;
        }
    }

    public void changeToTransparent(bool transparent)
    {
        MeshRenderer[] meshR_Sculpture = polys[newest].sculpture.transform.GetComponentsInChildren<MeshRenderer>();

        for (int i = 0; i < meshR_Sculpture.Length; i++)
        {
            Debug.Log("original Material of Child " + i + " = " + meshR_Sculpture[i].material.name);
            if (transparent)
            {
                polys[newest].materialsSave.Add(meshR_Sculpture[i].material);
                meshR_Sculpture[i].material = transparentMaterial;
            }
            else
            {
                meshR_Sculpture[i].material = polys[newest].materialsSave[i];
            }
            Debug.Log("ChangeToTransparent(" + transparent + ") -> Material changed to " + meshR_Sculpture[i].material.name);
        }
    }

    public void adjustSizeOfPolyObject()
    {
        MeshRenderer[] mR_arr = polys[newest].sculpture.GetComponentsInChildren<MeshRenderer>();
        Vector3 biggestSize = Vector3.zero;
        float biggest = 0;
        foreach (MeshRenderer mR in mR_arr)
        {
            biggestSize = Vector3.Max(biggestSize, mR.bounds.size);
            Debug.Log("mR bounding size: " + mR.bounds.size);
        }
        biggest = Mathf.Max(biggestSize.x, biggestSize.y);
        biggest = Mathf.Max(biggest, biggestSize.z);
        float scaleFactor = 0.5f / biggest;
        Debug.Log("biggest bounding size: x = " + biggestSize.x + ", y = " + biggestSize.y + ", z = " + biggestSize.z);
        Debug.Log("localScale of Sculpture before adjustment is " + polys[newest].sculpture.transform.localScale);
        polys[newest].sculpture.transform.localScale *= scaleFactor;
        Debug.Log("localScale of Sculpture after adjustment is " + polys[newest].sculpture.transform.localScale);
    }

    public void adjustPositionOfPolyObject()
    {
        MeshRenderer[] mR_arr = polys[newest].sculpture.GetComponentsInChildren<MeshRenderer>();
        float yMin = 1;
        foreach (MeshRenderer mR in mR_arr)
        {
            if (yMin > mR.bounds.min.y)
                yMin = mR.bounds.min.y;
        }
        Debug.Log("yMin = " + yMin);
        polys[newest].sculpture.transform.position = new Vector3(Vector3.zero.x, -yMin, Vector3.zero.z);
    }

    public void PrepareForLeanTouch()
    {
        CapsuleCollider collider = polys[newest].sculpture.AddComponent<CapsuleCollider>();
        LeanSelectable sculptureSelectable = polys[newest].sculpture.AddComponent<LeanSelectable>();
        LeanSelectable markerSelectable = polys[newest].marker.GetComponent<LeanSelectable>();
        sculptureSelectable.OnSelect.AddListener(markerSelectable.Select);
        sculptureSelectable.OnDeselect.AddListener(markerSelectable.Deselect);
    }

    public void initPolyObject()
    {
        int loadedPolys = objectCarrier.transform.childCount;
        if (loadedPolys > 0)
        {
            polys.Add(new Poly());
            newest = polys.Count - 1;
            polys[newest].sculpture = Instantiate(objectCarrier.transform.GetChild(loadedPolys - 1).gameObject, Vector3.zero, new Quaternion(0, 0, 0, 0));
            polys[newest].sculpture.name = "sculpture " + newest;

            adjustSizeOfPolyObject();
            adjustPositionOfPolyObject();
            changeToTransparent(true);

            polys[newest].marker = Instantiate(hitMarker, Vector3.zero, new Quaternion(0, 0, 0, 0));

            polys[newest].marker.SetActive(false);
            polys[newest].sculpture.transform.SetParent(polys[newest].marker.transform);

            PrepareForLeanTouch();

            alreadyHitHorizontalPlane = false;
            polyInitiated = true;

            Debug.Log("New Sculpture[" + newest + "] Instantiated -> name = " + polys[newest].sculpture.name);
            Debug.Log("childcount of objectCarrier = " + loadedPolys);
        }
        else
        {
            Debug.LogError("Object Carrier has no child!");
        }

    }
    GameObject sceneManager;
    SceneLoader sceneLoader;

    private void Awake()
    {
        arRaycastManager = GetComponent<ARRaycastManager>();
        uiController = new UIController();
        sceneManager = GameObject.FindGameObjectWithTag("Manager");
        sceneLoader = sceneManager.GetComponent<SceneLoader>();
    }
    private void Start()
    {
        objectCarrier = GameObject.FindWithTag("DontDestroy");
        if (objectCarrier != null)
        {
            initPolyObject();
        }
        else
        {
            Debug.Log("Object Carrier not found -> Object Carrier = " + objectCarrier.name);
        }

        firstTime = false;
    }

    private void OnEnable()
    {
        if (SceneManager.GetSceneByName("Augmenting Masterpieces").isLoaded)
        {
            pictureModeButton.SetActive(sceneLoader.IsExperimentalModeActive());
        }
    }

    private void OnDisable()
    {
        timer = 1f;
        polyInitiated = false;
    }

    private void Update()
    {
        if (!firstTime && !polyInitiated)
        {
            timer -= Time.deltaTime;
            if (timer < 0) //wait timer so polyobject is guaranteed to be loaded
            {
                initPolyObject(); //First Polyinitiation after Scene is reactived
            }
        }


        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
        {
            uiTouched = uiController.IsTouchOverUIObject(Input.GetTouch(0).position);
            if (!uiTouched)
            {
                if (arRaycastManager.Raycast(Input.GetTouch(0).position, hits, TrackableType.PlaneEstimated))
                {
                    OnTouchAndRaycastHitHorizontalARPlane(hits);
                }
            }
        }

        if (arRaycastManager.Raycast(screenMiddle, hits, TrackableType.PlaneWithinPolygon))
        {
            OnRaycastHitHorizontalARPlane(hits);
        }
    }
}
