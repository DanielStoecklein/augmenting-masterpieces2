﻿using UnityEngine;
//Author: Daniel Stöcklein

/// <summary>This class handles Object-Selection</summary>
public class Selectable : MonoBehaviour
{
    private GameObject selectable; //Contains 'Select' Material

    /// <summary>Selects an GameObject, if it has the 'Selected' Material as child. </summary>
    public void Select(GameObject obj)
    {
        if (obj == null) return;

        if (IsSelectable(obj)) //Is object selectable?
        {
            if (!IsAlreadySelected()) //Is object already selected?
            {
                selectable.SetActive(true);
                obj.transform.Find("Hoehe").gameObject.SetActive(true);
                obj.transform.Find("Breite").gameObject.SetActive(true);
            }
        }
        else
        {
            Debug.Log("Kein Select Material gefunden!");
        }
    }

    /// <summary>Deselects an GameObject, if it has the 'Selected' Material as child. </summary>
    public void Deselect(GameObject obj)
    {
        if (obj == null) return;

        if (IsSelectable(obj)) //Is object selectable?
        {
            if (IsAlreadySelected()) //Is object already selected?
            {
                selectable.SetActive(false);
                obj.transform.Find("Hoehe").gameObject.SetActive(false);
                obj.transform.Find("Breite").gameObject.SetActive(false);
            }
        }
        else
        {
            Debug.Log("No selectable object found");
        }
    }


    /// <summary>Search for the 'Selected' Material. </summary>
    private bool IsSelectable(GameObject obj)
    {
        GameObject child = TagSearcher.FindObjectsWithTag(obj.transform, "Selected");
        {
            if (child != null)
            {
                selectable = child.transform.gameObject;
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    /// <summary>Checks if the 'Selected' Material is active. </summary>
    private bool IsAlreadySelected()
    {
        if (selectable.activeSelf)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}



