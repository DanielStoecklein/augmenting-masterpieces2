﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;

[RequireComponent(typeof(ARPlaneManager))]
public class PlaneSettings : MonoBehaviour
{
    private ARPlaneManager arPlaneManager;
    private MeshRenderer meshRenderer;
    private LineRenderer lineRenderer;
    [SerializeField]
    private bool adjustPlanes;

    [SerializeField]
    private GameObject plane;

    [SerializeField]
    private bool visible;

    List<ARPlane> arPlanes;
    ARPlane planeWithLowestY;
    float lowestY = 420;

    private void Action_planesChanged(ARPlanesChangedEventArgs eventArgs)
    {
        if (eventArgs.added.Count > 0)
        {
            Debug.Log("Action: Planes Added!");
            arPlanes = eventArgs.added;
            foreach (var plane in arPlanes)
            {
                Debug.Log("   new plane: position = " + plane.transform.position);
                if (lowestY == 420 || lowestY > plane.transform.position.y)
                {
                    lowestY = plane.transform.position.y;
                    planeWithLowestY = plane;
                }
                else if (lowestY < plane.transform.position.y)
                {
                    plane.transform.position = new Vector3(plane.transform.position.x, lowestY, plane.transform.position.z);
                    Debug.Log("   Height of new Plane adjusted to " + plane.transform.position.y);
                }
            }


        }

    }

    // Start is called before the first frame update
    void Start()
    {
        arPlaneManager = GetComponent<ARPlaneManager>();
        meshRenderer = plane.GetComponent<MeshRenderer>();
        lineRenderer = plane.GetComponent<LineRenderer>();
        SetPlaneVisibility(visible);
        if(adjustPlanes)
            arPlaneManager.planesChanged += Action_planesChanged;
    }
    private void TogglePlaneManager()
    {
        arPlaneManager.enabled = !arPlaneManager.enabled;

        foreach (ARPlane plane in arPlaneManager.trackables)
        {
            plane.gameObject.SetActive(arPlaneManager.enabled);
        }
    }

    private void SetPlaneDetection(bool state)
    {
        arPlaneManager.enabled = state;
    }

    private void SetPlaneActivation(bool state)
    {
        foreach (ARPlane plane in arPlaneManager.trackables)
        {
            plane.gameObject.SetActive(state);
        }
    }

    private void SetPlaneVisibility(bool visible)
    {
        if (meshRenderer != null)
            meshRenderer.enabled = visible;
        if (lineRenderer != null)
            lineRenderer.enabled = visible;
        Debug.Log("Visibility of the Planes changed to " + visible.ToString());
    }

    void Update()
    {

    }

    void OnDestroy()
    {
        if(adjustPlanes)
            arPlaneManager.planesChanged -= Action_planesChanged;
    }
}
