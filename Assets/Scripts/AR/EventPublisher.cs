﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

//Author: Simon Weickert

public class EventPublisher : MonoBehaviour
{
    public event EventHandler OnTouchBegan;
    public event arRaycastEventWithHits OnRaycastHitHorizontalARPlane;
    // public event arRaycastEventWithHits OnRaycastHitVerticalARPlane; Vertical AR Plane Functionality Removed
    // public event arRaycastEventWithHits OnTouchAndRaycastHitVerticalARPlane; Vertical AR Plane Functionality Removed

    public event arRaycastEventWithHits OnTouchAndRaycastHitHorizontalARPlane;
    public event RaycastEventWithHitobject OnTouchAndRaycastHitPhysicsObject;
    public delegate void arRaycastEventWithHits(List<ARRaycastHit> hitList);
    public delegate void RaycastEventWithHitobject(RaycastHit hitObject);

    private Camera arCamera;
    private Vector2 screenMiddle = new Vector2(Screen.width / 2, Screen.height / 2);
    private List<ARRaycastHit> hits = new List<ARRaycastHit>();
    private ARRaycastManager arRaycastManager;
    private ARPlaneManager arPlaneManager;

    private UIController uiController;
    private bool uiTouched;
    RaycastHit hitObject;

    void Start()
    {
        arCamera = Camera.main;
        arRaycastManager = GetComponent<ARRaycastManager>();
        arPlaneManager = GetComponent<ARPlaneManager>();
        uiController = new UIController();
    }

    void Update()
    {

        //Event: Touchinput at Beganphase
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
        {
            uiTouched = uiController.IsTouchOverUIObject(Input.GetTouch(0).position);
            if (!uiTouched)
            {
                OnTouchBegan?.Invoke(this, EventArgs.Empty);
                if (arRaycastManager.Raycast(Input.GetTouch(0).position, hits, TrackableType.PlaneEstimated))
                {

                    OnTouchAndRaycastHitHorizontalARPlane?.Invoke(hits);

                }
                if (Physics.Raycast(arCamera.ScreenPointToRay(Input.GetTouch(0).position), out hitObject))
                {
                    OnTouchAndRaycastHitPhysicsObject?.Invoke(hitObject);
                }
            }
        }

        if (arRaycastManager.Raycast(screenMiddle, hits, TrackableType.PlaneEstimated)) //hits will only contain AR Planes
        {
            OnRaycastHitHorizontalARPlane?.Invoke(hits);

        }

    }
}
